var urlBase = 'http://localhost/inventar/';
// https://inventar.mokos-test.com/


var dataTableLanguage = {
    "emptyTable":     "Nema podataka u tablici",
    "lengthMenu":     "Prikaži _MENU_ rezultata",
    "info":           "",
    "infoEmpty":      "",
    "search":         "Pretraži:",
        "paginate": {
            "first":      "Prva",
            "last":       "Posljednja",
            "next":       "Sljedeća",
            "previous":   "Prethodna"
        },
}


$(document).ready( function () {

    $("#selectWarehosue").submit(
        function(event) {      
            var form = $(this);
            // prevent default form submit  
            event.preventDefault();

            // get the endpoint
            var endpoint = form.attr('action');
            var value = $("#warehouseSelectBox").val();
            // get field value 
            window.location.href = endpoint + '/' + value;
        }
      );

    
    var inventoryList = $('#inventoryList').DataTable( {
        language: dataTableLanguage,
        lengthChange: false,
        responsive: {
            details:{
                display: $.fn.dataTable.Responsive.display.childRowImmediate,
                type: 'none',
                target:''
            }
        },
        paging: false,
        lengthMenu: [
            [25, 50, 100, 200, -1],
            [25, 50, 100, 200, "All"]
        ],
        displayLength: -1,
        columnDefs: [
            {
                "targets": -1,
                "searchable": false,
                "orderable": false,
                "className": 'dt-body-right',
                
            },
        ]
    } );


    var userTable = $('#users_table').DataTable({
        "language": dataTableLanguage,
        responsive: true,
        "columnDefs": [
            {
                "targets": -1,
                "searchable": false,
                "orderable": false,
                "className": 'dt-body-right',
                
            },
            { responsivePriority: 1, targets: 0 },
            { responsivePriority: 2, targets: -1 }
        ]
        
    });

    $('#product_category_table').DataTable({
        language: dataTableLanguage,
        responsive: true,
        columnDefs: [
            {
                "targets": -1,
                "searchable": false,
                "orderable": false,
                "className": 'dt-body-right',
                
            },
            { responsivePriority: 1, targets: 1 },
            { responsivePriority: 2, targets: -1 },
        ]
    });

    $('#product_table').DataTable({
        "language": dataTableLanguage,
        responsive: true,
        lengthMenu: [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, "All"]
        ],
        columnDefs: [
            {
                "targets": -1,
                "searchable": false,
                "orderable": false,
                "className": 'dt-body-right',
                
            },
            
            { responsivePriority: 1, targets: -1 }
        ]
    });

    $('#inventory_report').DataTable({
        "language": dataTableLanguage,
        responsive: true,
        columnDefs: [
            {
                "targets": -1,
                "searchable": false,
                "orderable": false,
                "className": 'dt-body-right',
                
            },
            
            { responsivePriority: 1, targets: -1 }
        ]
    });


    $(".btn-delete").click(
        function(event) {
            event.preventDefault();

            $('#delete-modal').modal({});
            var finalLink = $('#delete-user-final-link');
            finalLink.attr('href', $(event.target).attr('href'));
        }
      );

      $(".btn-finish").click(
        function(event) {
            event.preventDefault();

            $('#finish-modal').modal({});
            var finalLink = $('#finish-final-link');
            finalLink.attr('href', $(event.target).attr('href'));
        }
      );


    $('.navbar-toggler').click(function() {
        $('#sidebarMenu').toggleClass('sidebar-open');
      });

      $('#stockFormSubmit').click(function(event){        
        event.preventDefault();
        $('#edit-modal').modal({});
                
      });

      $('#edit-invenotry').click(function(){        
      $('#stockForm').submit();
      
    });

      

    // Automatic state update handler

    var valueBefore;


    $(".auto-update input").focus(function(event){        
        target = $(event.target)[0];
        valueBefore = target.value;
    });

    
    $(".auto-update input").blur(function(event){
        target = $(event.target)[0];
        id = target.id;
        id = id.split('-')[1];
        valueAfter = target.value;
        if(valueBefore != valueAfter){
            completeURL = urlBase + 'inventory/editlist/'
            $.ajax({
                type: 'POST',
                url: completeURL,
                data: { 
                    'id': id, 
                    'value': valueAfter,
                },
                success: function(msg){
                    var parent = $(target).parent().parent();
                    var errorContainer = parent.find('.auto-update-error');                    
                    msg = JSON.parse(msg);
                    if(!msg.success){
                        errorContainer.html('Vrijednost mora biti 0 ili više.');

                    }else{
                        errorContainer.html('');
                        // provjeri postoje li prazna polja i postoji li blok
                        complete = true;
                        var itemLIst = $(".auto-update input");                        
                        itemLIst.each(function(index){
                            var inputField = $(this);
                            var display = inputField.parent().parent().css('display');                            
                            if(display == 'none'){                                                            
                                return true;
                            }
                            if(!inputField.val() || inputField.val() < 1){
                                complete = false;
                            }
                        });

                        var advanceBlock = $('#advance');

                        if(complete){
                            advanceBlock.removeClass('d-none');                       
                            advanceBlock.addClass('d-block');
                        }else{
                            advanceBlock.addClass('d-none');
                            advanceBlock.removeClass('d-block');
                        }
                    }
                }
            });
        } 

    });
} );
