var currentTime = new Date();
var day = String(currentTime.getDate()).padStart(2, '0');
var month = String(currentTime.getMonth() + 1).padStart(2, '0');
var year = currentTime.getFullYear();
var hour = currentTime.getHours();
var minutes = currentTime.getMinutes();

var dataTableLanguage = {
    "emptyTable":     "Nema podataka u tablici",
    "lengthMenu":     "Prikaži _MENU_ rezultata",
    "info":           "",
    "infoEmpty":      "",
    "search":         "Pretraži:",
        "paginate": {
            "first":      "Prva",
            "last":       "Posljednja",
            "next":       "Sljedeća",
            "previous":   "Prethodna"
        },
}


$(document).ready(function(){
    
var table = $('#example').DataTable( {
    language: dataTableLanguage,
    lengthChange: false,
    responsive: true,
    buttons: ['excel', 'pdf']
} );

table.buttons().container()
    .appendTo( '#example_wrapper .col-md-6:eq(0)' );

if($('#inventorySummary').length){
    var inventorySummary = $('#inventorySummary').DataTable( {
        language: dataTableLanguage,
        lengthChange: false,
        responsive: true,
        columnDefs: [{"targets": [6],
            render: function ( data, type, row ) {
              color = 'green';
              if (data == 'NE') {
                color = 'red';
              } 
              return '<span style="color:' + color + '">' + data + '</span>';
            }
       }],
    } );


}


var inventoryReport = $('#inventoryReport').DataTable( {
    language: dataTableLanguage,
    lengthChange: false,
    responsive: true,
} );




});