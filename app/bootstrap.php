<?php

use Libraries\Router\Router;

// Load Config
require_once 'config/config.php';
require_once '../vendor/autoload.php';

// SPL auto loader
spl_autoload_register(function ($className) {
    $className = explode('\\', $className);
    $class = array_pop($className);
    $className = array_map('strtolower', $className);
    $className[] = $class;
    $className = implode('/', $className);
    $className =  str_replace("\\", "/", $className) . ".php";
    if (is_file(APP_ROOT . '/' . $className)) {
        require_once  $className;
    }
});


// Check if url was passed
if (!empty($_GET['url'])) {
    $url = $_GET['url'];
} else {
    $url = '';
}

// Parse the url
$router = new Router($url);

// Check if url is correct, instantiate MVC components
$route = $router->find();

// Get model, view and controller
$view = $route->getView();
$controller = $route->getController();
$action = $router->getAction();
$params = $router->getParams();

if (!empty($controller) && !empty($action)) {
    if (method_exists($controller, $action)) {
        $controller->$action($params);
    }
}

if (!empty($view)) {
    $view->renderTemplate();
}

// If View and controller are not found, render custom 404
if (empty($view) && empty($controller)) {

    $endpoint = '404';

    @list($m, $v, $c) = CUSTOM_ROUTS[$endpoint];

    // Set the custom route by using ROUTE overide array
    $route = $router->setCustomMVC($m, $v, $c);
    $view = $route->getView();

    $controller = $route->getController();
    $action = $router->getAction();
    $params = $router->getParams();

    if (!empty($controller) && !empty($action)) {
        if (method_exists($controller, $action)) {
            $controller->$action($params);
        }
    }

    if (!empty($view)) {
        $view->renderTemplate();
    }
}
