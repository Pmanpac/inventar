<?php

namespace Libraries\Core;

class BaseView{

    const TEMPLATE = 'template';
    protected $pageTitle = SITE_NAME;
    protected $model;
    protected $session;

    public function __construct($model = null){
        $this->model = $model;
        $this->session = new Session;
    }

    public function renderTemplate(){
        require_once self::TEMPLATE . ".php";
    }


    public function get_title(){
        $return = SITE_NAME;

        if (!empty($this->pageTitle) && $this->pageTitle !== SITE_NAME){
            $return = $return . " - " . $this->pageTitle;
        }

        return $return;
    }

}