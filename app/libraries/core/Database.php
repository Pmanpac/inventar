<?php

namespace Libraries\Core;

use PDO;
use PDOException;

/**
 * Undocumented class
 */
class Database{

    // Database properties 

    // Conection constants 
    private $host = DB_HOST;
    private $user = DB_USER;
    private $pass = DB_PASS;
    private $dbname = DB_NAME;

    // Connection config constants
    private const PDO_OPTIONS = array(
        PDO::ATTR_PERSISTENT => true,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
    );

    private $con;
    private $stmt;
    private $error;
    
    // Database constructor
    public function __construct(){
        
        $dsn = "mysql:host=" . $this->host . ";dbname=" . $this->dbname;

        try{
            $this->con = new PDO($dsn, $this->user, $this->pass, self::PDO_OPTIONS);
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            die($this->error);
        }
    }

    // Database Methods
    // Prepare statement with query
    public function query ($sql){
        $this->stmt = $this->con->prepare($sql);

        return $this;
    }

    // Bind values
    public function bind($param, $value, $type = null){

        if(is_null($type)){
            switch(true){
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;
            }
        }

        $this->stmt->bindValue($param, $value, $type);

        return $this;
    }

    // Execute the prepared statement
    public function execute(){
        if(!empty($this->stmt)){
            try{
                $result =  $this->stmt->execute();
                return $result;
            }catch(PDOException $e){
                $this->error = $e->getMessage();
                die($this->error);
            }
            
        }

        return false;
    }

    // Get result set as array of objects
    public function getAll(){
        if(!empty($this->stmt) && $this->execute()){

            return $this->stmt->fetchAll(PDO::FETCH_OBJ);

        }

        return false;
    }

    // Get a single row as a object
    public function getRow(){
        if(!empty($this->stmt) && $this->execute()){

            return $this->stmt->fetch(PDO::FETCH_OBJ);
            
        }

        return false;
    }

    // Get last insert id
    public function getLastInsertId(){
        $id = $this->con->lastInsertId();

        return $id;
    }

    // Get a row count
    public function rowCount(){
        if (!empty($this->stmt)){
            return $this->stmt->rowCount();
        }

        return false;
    }
}