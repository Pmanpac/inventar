<?php

namespace Libraries\Core;

use Exception;
use Libraries\Core\SessionInterface;

class Session implements SessionInterface{

    public function __construct(){
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
         }
    }

    public function get($key){
        if($this->exists($key)){
            return $_SESSION[$key];
        }

        return false;
    }

    public function set($key, $value){
        $_SESSION[$key] = $value;
    }

    public function exists($key){
        if(!empty($_SESSION[$key])){
            return true;
        }

        return false;
    }

    public function remove($key){
        if($this->exists($key)){
            unset($_SESSION[$key]);
        }
    }

    public function count(){
        return count($_SESSION);
    }

    public function destroy(){
        session_destroy();
    }

    public function flash($key, $value = '', $class = ''){
        if($this->exists($key) && !empty($this->get($key))){
            echo '<div class="' . $this->get($key . '_class') . '" id="msg-flash" >' . $this->get($key) . '</div>';
            $this->remove($key);
            $this->remove($key . '_class');
        }else if(!$this->exists($key) && !empty($value)){
            $this->set($key, $value);
            $this->set($key . '_class', $class);
        }
    }

}