<?php 

namespace Libraries\Core;

class FormModel{
    protected $formData;
    protected $formDataError;

    public function __construct($formData, $formDataError)
    {
        $this->formData = $formData;
        $this->formDataError = $formDataError;
    }


    public function setFromData($data){
        foreach($data as $key => $value){
            if(in_array($key, array_keys( $this->formData))){
                $this->formData[$key] = $value;
            }
        }
    }

    public function setFormDataError($data){
        foreach($data as $key => $value){
            if(in_array($key, array_keys( $this->formDataError))){
                $this->formDataError[$key] = $value;
            }
        }
    }

    public function getFormData(){
        return $this->formData;
    }

    public function getFormDataError(){
        return $this->formDataError;
    }
}