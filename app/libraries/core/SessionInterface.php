<?php

namespace Libraries\Core;


interface SessionInterface{

    public function get($key);

    public function set($key, $value);

    public function exists($key);

    public function remove($key);

    public function count();

    public function flash($key, $value = '', $class = '');
}