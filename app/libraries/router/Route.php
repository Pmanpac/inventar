<?php

namespace Libraries\Router;

class Route {

    private $model;
    private $view;
    private $controller;
    

    public function __construct($controller = null, $model = null, $view = null) {

        $this->view = $view;
        $this->controller = $controller;

    }

    public function getView() {
        return $this->view;
    }

    public function getController() {
        return $this->controller;
    }

    public function getModel() {
        return $this->model;
    }

}