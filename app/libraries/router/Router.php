<?php

namespace Libraries\Router;
use Libraries\Router\Route;

class Router {

    private $url;
    private $componentName;
    private $actionName;
    private $params;

    public function __construct($url){
        $this->url = $url;
        $this->parseURL($this->url);
    }

    /**
     * Function parseURL splits urls params
     *
     * @param string $url
     * @return void
     */
    private function parseURL(string $url){

        // parse the url
        $url = trim(parse_url($url, PHP_URL_PATH), "/");
            
        $url = filter_var($url, FILTER_SANITIZE_URL);

        // set up app component, action and params from passed from url
        @list($component, $action, $params) = explode("/", $url, 3);

        
        if (!empty($component)) {
            $this->setComponent($component);
        } elseif (!empty(ROUTER[$url])) {
            $this->setComponent(ROUTER[$url]);
        }
        if (isset($action)) {
            $this->setAction($action);
        }
        if (isset($params)) {
            $this->setParams(explode("/", $params));
        }
    }

    /**
     * Function find instantiates Model, View, Controller based on url params
     */
    public function find() :Route {

        $model = [];
        $view = null; 
        $controller = null;
        
        if(!empty($this->componentName)){

            $modelNamespace = "\\Models\\" . ucfirst($this->componentName);
            
            // If class exists, instantiate Model
            if(class_exists($modelNamespace)){
                $model = new $modelNamespace();
            }
            
            $viewNamespace = "\\Components\\" . ucfirst($this->componentName);
            
            $controllerNamespace = $viewNamespace;
            
        }

        if(!empty($this->actionName)){
            $viewNamespace .=  "\\" . ucfirst($this->actionName);
            $controllerNamespace .= "\\" . ucfirst($this->actionName);
        }

        if(!empty($viewNamespace) && !empty($controllerNamespace)){
            $viewNamespace .= "\\" . "View";
            $controllerNamespace .= "\\" . "Controller";

            // If class exists, instantiate View
            if(class_exists($viewNamespace)){
                $view = new $viewNamespace($model);
            }

            // If class exists, instantiate Controller
            if(class_exists($controllerNamespace)){
                $controller = new $controllerNamespace($model);
            }
        }

        // If conventional component is not found, try to find a custom overide
        if(empty($viewNamespace) && empty($controller)){
            if(!empty(CUSTOM_ROUTS[$this->componentName])){
                @list($modelNamespace, $viewNamespace, $controllerNamespace) = CUSTOM_ROUTS[$this->componentName];
                return $this->setCustomMVC($modelNamespace, $viewNamespace, $controllerNamespace);
            }
        }

        return new Route($controller, $model, $view);
    }

    public function setCustomMVC($model = null, $view, $controller = null){
        // Resolve the model
        if(class_exists($model)){
            $model = new $model();
        }

        // Resolve the view
        if(class_exists($view)){
            $view = new $view($model);
        }

        if(class_exists($controller)){
            $controller = new $controller($model);
        }

        return new Route($controller, $model, $view);
    }

    public function setComponent($component){
        $this->componentName = $component;
    }

    public function setAction($action){
        $this->actionName = $action;
    }

    public function setParams($params){
        $this->params = $params;
    }

    public function getComponent(){
        return $this->componentName;
    }

    public function getAction(){
        return $this->actionName;
    }

    public function getParams(){
        return $this->params;
    }

}