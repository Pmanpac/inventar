<?php 
    use Libraries\Core\Session; 
    $session = new Session();
    $session->flash("user_login_success");
?>

<nav class="navbar navbar-dark sticky-top bg-dark navbar-expand-lg  p-0">
    
    <button class="navbar-toggler d-sm-block d-lg-none">
        <span class="navbar-toggler-icon"></span>
    </button>

    <a class="navbar-brand pl-3" href="#">SKLADIŠTE</a>

    <ul class="navbar-nav ml-auto px-3">
        <li class="nav-item text-nowrap">
            <a class="nav-link" href="<?php echo URL_BASE ?>user/profile/<?php echo $session->get('userId') ?>"> <i class="fa fa-user"></i> Korisnik: <?php echo $session->get('userName') ?></a>
        </li>
        <li class="nav-item text-nowrap">
            <a class="nav-link" href="<?php echo URL_BASE ?>user/logout">Odjava</a>
        </li>
    </ul>
</nav>

<div class="container-fluid">
    <div class="row">
    <nav id="sidebarMenu" class="col-lg-2 col-lg-2-custom d-none d-lg-block bg-light sidebar navbar navbar-expand-lg">
        <div class="sidebar-sticky">
            <ul class="nav flex-column">
                <li class="nav-item">
                    <a class="nav-link active" href="<?php echo URL_BASE ?>dashboard/welcome">
                        <i class="fa fa-tachometer"></i> Upravljačka Ploča
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="<?php echo URL_BASE ?>warehouse/overview">
                        <i class="fa fa-home"></i></span> Skladišta
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo URL_BASE ?>product/overview">
                        <i class="fa fa-shopping-cart"></i> Proizvodi
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo URL_BASE ?>category/overview">
                        <i class="fa fa-th-list"></i></i> Kategorije
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo URL_BASE ?>report/overview">
                        <i class="fa fa-file"></i> Izvještaji
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="<?php echo URL_BASE ?>inventory/overview">
                        <i class="fa fa-signal"></i></span> Inventura
                    </a>
                </li>
                <?php $session = new Session; if($session->get('userGroup') != 2): ?>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo URL_BASE ?>user/overview">
                            <i class="fa fa-user"></i> Korisnici
                        </a>
                    </li>
                <?php endif ?>
            </ul>
        </div>
    </nav>