<?php

use Libraries\Core\Session;

$session = new Session 

?>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-dash-3">
    <div class="container">
        <a class="navbar-brand" href="<?php echo URL_BASE ?>"><?php echo SITE_NAME ?></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="<?php echo URL_BASE ?>">Naslovna</a>
            </li>
            </ul>
            
            <ul class="navbar-nav ml-auto">

            <?php if($session->exists('userId')): ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo URL_BASE ?>user/logout">Odjava</a>
                </li>
            <?php else: ?>
            <!-- <li class="nav-item">
                <a class="nav-link" href="<?php echo URL_BASE ?>user/register">Register</a>
            </li> -->
            <li class="nav-item">
                <a class="nav-link" href="<?php echo URL_BASE ?>user/login">Prijava</a>
            </li>

            <?php endif ?>
            </ul>
        </div>
  </div>
</nav>