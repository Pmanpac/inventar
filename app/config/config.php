<?php

// DISABLE ALL ERROR REPORTING
// error_reporting(0);

// error_reporting(E_ERROR | E_PARSE);

// SITE NAME
define('SITE_NAME', 'INVENTAR');

// DB CONFIG
define ('DB_HOST', 'localhost');
define ('DB_USER', 'root');
define ('DB_PASS', '');
define ('DB_NAME', 'inventar_db');

// APP ROOT
define('APP_ROOT', dirname(__DIR__));

// URL ROOT
define('URL_BASE', 'http://localhost/inventar/');

// TEMPLATE INCLUDES ROOT
define('INCLUDES', APP_ROOT . "/includes");


// ROUTER OVERIDE
// Overide the routed default convention based setup
define('ROUTER', array(
    '' => 'Home',
));

// ROUTER OVERIDE
// Routs not defined by convention rules
define('CUSTOM_ROUTS', array(
    '404' => [null, '\\Components\\ErrorCode\\ErrorNotFound\\View', null],
));


// START SESSION
session_start();




