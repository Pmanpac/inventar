<?php 


namespace Models;

use Libraries\Core\Database;
use Libraries\Core\FormModel;

class Inventory{
    
    private $db;
    public $formModel;
    private $editInventoryId;
    private $listInvetoryId;

    public function __construct(){
        $this->db = new Database;

        $this->formModel = new FormModel(
            ['name' => '', 'description' => ''],

            ['name' => '',]
        );
    }

    public function create($warehouse, $naziv, $opis){
        $sql = 'INSERT INTO inventura (id_skladiste, naziv, opis) VALUES (:warehouse, :naziv, :opis);';
        $result = $this->db->query($sql)->bind(':naziv', $naziv)->bind(':opis', $opis)->bind(':warehouse', $warehouse)->execute();

        if($result){
            $insertId = $this->db->getLastInsertId();
            // Get all products
            $products = $this->getProductState($warehouse);

            // If products returned, insert product state
            if($products){

                // Generate slq insert query
                $sql = 'INSERT INTO inventurni_zapis (inventura_id, proizvod_id, stanje_sustav) VALUES ';
                $placeholders = [];
                foreach ($products as $product){
                    $placeholders[] = "($insertId, $product->id, $product->kolicina)";
                }

                $sql .= implode(', ', $placeholders) . ';';

                if($this->db->query($sql)->execute()){
                    return $result;
                }

                return false;
                
            }
        }

        return false;
    }

    public function getAllActiveInventory(){
        $sql = 'SELECT inventura.*, skladiste.naziv as skladiste_naziv FROM inventura JOIN skladiste ON skladiste.id = inventura.id_skladiste WHERE inventura.aktivno = 1 AND skladiste.aktivno = 1 AND inventura.zakljucena = 0;';
        $result = $this->db->query($sql)->getAll();

        return $result;
    }

    public function getInventoryReports(){
        $sql = 'SELECT inventura.*, skladiste.naziv as skladiste FROM inventura JOIN skladiste ON skladiste.id = inventura.id_skladiste WHERE inventura.aktivno = 1 AND inventura.vrijeme_kraja IS NOT NULL AND inventura.zakljucena = 1;';
        $result = $this->db->query($sql)->getAll();

        return $result;
    }

    public function getAllWarehouses(){
        $sql = 'SELECT
        s.*,
        i.vrijeme_pocetka,
        i.vrijeme_kraja,
        i.aktivno,
        i.zakljucena
    FROM
        skladiste s
    JOIN skladiste s2 ON
        s.id = s2.id AND s.aktivno = 1
    LEFT JOIN inventura i ON
        i.id_skladiste = s.id
    WHERE
        s.aktivno = 1 AND(
            i.aktivno = 1 OR i.aktivno IS NULL
        ) AND i.id IN(
        SELECT
            MAX(a.id)
        FROM
            inventura a
        GROUP BY
            a.id_skladiste
    ) OR i.id IS NULL;';
        $result = $this->db->query($sql)->getAll();
        return $result;
    }

    public function getActiveInventoryCount(){
        $sql = 'SELECT COUNT(*) AS broj FROM inventura WHERE aktivno = 1 AND vrijeme_kraja IS NULL AND zakljucena = 0;';
        $result = $this->db->query($sql)->getRow();

        return $result;
    }

    public function getInventoryReportsCount(){
        $sql = 'SELECT COUNT(*) AS broj FROM inventura WHERE aktivno = 1 AND vrijeme_kraja IS NOT NULL AND zakljucena = 1;';
        $result = $this->db->query($sql)->getRow();

        return $result;
    }

    public function delete($id){

        $sql = 'DELETE FROM inventurni_zapis WHERE inventura_id = :id';
        $result = $this->db->query($sql)->bind(':id', $id)->execute();

        if($result){
            $sql = 'DELETE FROM inventura WHERE id = :id';
            $result = $this->db->query($sql)->bind(':id', $id)->execute();
        }
        
        return $result;
    }

    public function update($id, $naziv, $opis){
        $sql = 'UPDATE inventura SET naziv = :naziv, opis = :opis WHERE id = :id';
        $result = $this->db->query($sql)->bind(':id', $id)->bind(':naziv', $naziv)->bind('opis', $opis)->execute();

        return $result;
    }

    public function getById($id){
        $sql = 'SELECT * FROM inventura WHERE id = :id;';
        $result = $this->db->query($sql)->bind(':id', $id)->getRow();

        return $result;
    }

    public function readAll(){
        $sql = 'SELECT * FROM inventura WHERE aktivno = 1;';
        $result = $this->db->query($sql)->getAll();

        return $result;
    }

    public function getInventoryList($id){
        $sql = 'SELECT iz.id AS item_id, i.id, i.naziv, i.opis, iz.stanje_sustav, iz.stanje_skladiste FROM inventurni_zapis iz JOIN proizvod i ON i.id = iz.proizvod_id WHERE iz.inventura_id = :id';
        $result = $this->db->query($sql)->bind(':id', $id)->getAll();

        return $result;
    }

    public function updateInventoryState($id, $value){
        $sql = 'UPDATE inventurni_zapis SET stanje_skladiste = :val WHERE id = :id';
        $result = $this->db->query($sql)->bind(':val', $value)->bind(':id', $id)->execute();

        return $result;
    }

    public function getInventorySummary($id){
        $sql = 'SELECT iz.id AS item_id, i.id, i.naziv, i.cijena, i.kalo, iz.stanje_sustav, iz.stanje_skladiste FROM inventurni_zapis iz JOIN proizvod i ON i.id = iz.proizvod_id WHERE iz.inventura_id = :id';
        $result = $this->db->query($sql)->bind(':id', $id)->getAll();

        return $result;
    }

    public function getWarehouseId($id){
        $sql = 'SELECT id_skladiste FROM inventura WHERE id = :warehouse;';
        $result = $this->db->query($sql)->bind(':warehouse', $id)->getRow();

        return $result;
    }

    public function getWarehouseById($id){
        $sql = "SELECT skladiste.*, korisnik.* FROM skladiste JOIN inventura ON skladiste.id = inventura.id_skladiste JOIN korisnik ON skladiste.upravitelj_id = korisnik.id WHERE inventura.id = :id;";
        $result = $this->db->query($sql)->bind(':id', $id)->getRow();

        return $result;
    }

    public function correctSystemValues($id){
        $products = $this->getInventorySummary($id);
        $warehouse = $this->getWarehouseId($id)->id_skladiste;

        foreach($products as $product){
            // ako nema u sustavu a ima u skladistu
            if($product->stanje_sustav == 0){
                $sql = "INSERT INTO zaliha (id_proizvod, id_skladiste, kolicina) VALUES ($product->id, $warehouse, $product->stanje_skladiste);";
                $result = $this->db->query($sql)->execute();
            }else{
                $sql = "UPDATE zaliha SET kolicina = " . $product->stanje_skladiste . " WHERE id_proizvod = " . $product->id . " AND id_skladiste = " . $warehouse . ";";
                $result = $this->db->query($sql)->execute();
            }

            if(!$result){
                return false;
            }
        }

        return $result;
        
    }

    public function finish($id){
        $sql = "UPDATE inventura SET vrijeme_kraja = now(), zakljucena = 1 WHERE id = :id";
        $result = $this->db->query($sql)->bind(':id', $id)->execute();

        return $result;
    }

    private function getAllProducts($warehosue){
        $sql = 'SELECT proizvod.*, 0 AS kolicina FROM proizvod WHERE proizvod.aktivno = 1 AND proizvod.id NOT IN (SELECT zaliha.id_proizvod FROM zaliha WHERE zaliha.id_skladiste = :warehouse);';
        $result = $this->db->query($sql)->bind(":warehouse", $warehosue)->getAll();

        return $result;
    }


    public function getProductState($warehosue){
        $sql = 'SELECT proizvod.*, zaliha.kolicina AS kolicina FROM proizvod JOIN zaliha ON proizvod.id = zaliha.id_proizvod WHERE proizvod.aktivno = 1 AND zaliha.id_skladiste = :warehouse;';
        $result1 = $this->db->query($sql)->bind(":warehouse", $warehosue)->getAll();
        $result2 = $this->getAllProducts($warehosue);

        $result = array_merge($result1, $result2);
        

        return $result;
    }

    public function setEditInventoryId($id){
        $this->editInventoryId = $id;
    }

    public function getEditInventoryId(){
        return $this->editInventoryId;
    }

    public function setInventoryListId($id){
        $this->listInvetoryId = $id;
    }

    public function getInventoryListId(){
        return $this->listInvetoryId;
    }
}