<?php 


namespace Models;

use Libraries\Core\Database;
use Libraries\Core\FormModel;

class Warehouse{
    
    private $db;
    public $formModel;
    private $editWarehouseId;
    private $userModel;
    private $errorFields = [];

    public function __construct(){
        $this->db = new Database;
        $this->userModel = new User();
        $this->formModel = new FormModel(
            ['name' => '', 'pbr' => '', 'city' => '', 'address'=> '', 'description' => '', 'manager' => ''],

            ['name' => '', 'pbr' => '', 'manager' => '', 'city' => '', 'address'=> '',]
        );
    }

    public function create($naziv, $city, $pbr, $address, $opis, $upravitelj){
        $sql = 'INSERT INTO skladiste (naziv, grad, pbr, adresa, opis, upravitelj_id) VALUES (:naziv, :grad, :pbr, :adresa, :opis, :upravitelj);';
        $result = $this->db->query($sql)->bind(':naziv', $naziv)
                                        ->bind(':grad', $city)
                                        ->bind(':pbr', $pbr)
                                        ->bind(':adresa', $address)
                                        ->bind(':opis', $opis)
                                        ->bind(':upravitelj', $upravitelj)
                                        ->execute();

        if($result){
            return $result;
        }

        return false;
    }

    public function delete($id){

        $sql = 'UPDATE skladiste SET aktivno = 0 WHERE id = :id';
        $result = $this->db->query($sql)->bind(':id', $id)->execute();
        
        return $result;
    }

    public function update($id, $naziv, $city, $pbr, $address, $opis, $upravitelj){
        $sql = 'UPDATE skladiste SET naziv = :naziv, grad = :grad, pbr = :pbr, adresa = :adresa, opis = :opis, upravitelj_id = :upravitelj WHERE id = :id';
        $result = $this->db->query($sql)->bind(':id', $id)
                                        ->bind(':naziv', $naziv)
                                        ->bind(':grad', $city)
                                        ->bind(':pbr', $pbr)
                                        ->bind(':adresa', $address)
                                        ->bind('opis', $opis)
                                        ->bind(':upravitelj', $upravitelj)
                                        ->execute();

        return $result;
    }

    public function getById($id){
        $sql = 'SELECT * FROM skladiste WHERE id = :id;';
        $result = $this->db->query($sql)->bind(':id', $id)->getRow();

        return $result;
    }


    public function getByName($name){
        $sql = 'SELECT * FROM skladiste WHERE naziv = :name;';
        $result = $this->db->query($sql)->bind(':name', $name)->getAll();

        return $result;
    }

    public function checkName($id, $name){
        $sql = 'SELECT * FROM skladiste WHERE id != :id AND naziv = :name AND aktivno = 1;';
        $result = $this->db->query($sql)->bind(':id', $id)->bind(':name', $name)->getRow();

        return $result;
    }

    public function readAll(){
        $sql = 'SELECT skladiste.id, skladiste.naziv, skladiste.grad, skladiste.pbr, skladiste.adresa, skladiste.opis, skladiste.upravitelj_id, korisnik.ime, korisnik.email FROM skladiste JOIN korisnik ON skladiste.upravitelj_id = korisnik.id  WHERE skladiste.aktivno = 1;';
        $result = $this->db->query($sql)->getAll();

        return $result;
    }

    public function getUserById($id){
        $user = $this->userModel->getUserById($id);
        return $user;
    }

    public function getManagers(){
        $managers = $this->userModel->getManagers();
        return $managers;
    }

    public function getProducts($warehouseId){
        $sql = "SELECT 
                    proizvod.*,
                    zaliha.kolicina,
                    proizvod_kategorija.naziv AS 'kategorija' 
                FROM proizvod
                    LEFT JOIN zaliha ON proizvod.id = zaliha.id_proizvod AND zaliha.id_skladiste = :skladisteId
                    JOIN proizvod_kategorija ON proizvod_kategorija.id = proizvod.kategorija_id
                    JOIN skladiste ON zaliha.id_skladiste = skladiste.id
                WHERE proizvod.aktivno = 1
                    AND skladiste.aktivno = 1;";

        $products = $this->db->query($sql)->bind('skladisteId', $warehouseId)->getAll();
        
        return $products;
    }

    public function updateStock($warehouseId, $data){
        $sql = 'UPDATE zaliha SET kolicina = :stock WHERE id_skladiste = :warehouseId AND id_proizvod = :productId';
        foreach($data as $key => $value){
            $return = $this->db->query($sql)->bind(':stock', $value)->bind(':warehouseId', $warehouseId)->bind(':productId',$key)->execute();

            if (!$return){
                return $return;
            }
        }

        return true;
    }

    public function getEditWarehouseId(){
        return $this->editWarehouseId;
    }

    public function seteditWarehouseId($id){
        $this->editWarehouseId = $id;
    }

    public function addToErrorFields($key, $value){
        $this->errorFields[$key] = $value;
    }

    public function getErrorFields(){
        return $this->errorFields;
    }

    
}