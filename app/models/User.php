<?php 


namespace Models;

use Libraries\Core\Database;

class User{
    
    private $db;
    private $userData;
    private $userDataError;
    private $editUserId;

    public function __construct(){
        $this->db = new Database;

        $this->userData = [
            'name' => '',
            'email' => '',
            'password' => '',
            'confirm_password' => '',
            'administrator' => '',
        ];

        $this->userDataError = [
            'name' => '',
            'email' => '',
            'password' => '',
            'confirm_password' => '',
        ];
    }

    public function getUserByEmail(string $email){
        $sql = "SELECT * FROM korisnik WHERE email = :email";
        $user = $this->db->query($sql)->bind(':email', $email)->getRow();
        
        return $user;
    }

    public function chckEditMailUnique(string $email, int $id){
        $sql = "SELECT * FROM korisnik WHERE email = :email AND id != :id";
        $user = $this->db->query($sql)->bind(':email', $email)->bind(':id', $id)->getRow();
        
        if($user){
            return true;
        }

        return false;
    }

    public function getUserById($id){
        $sql = "SELECT * FROM korisnik WHERE id = :id";
        $user = $this->db->query($sql)->bind(':id', $id)->getRow();
        
        return $user;
    }

    public function getAllUsers($id){
        $sql = "SELECT * FROM korisnik WHERE id != :id AND aktivno = 1;";
        $users = $this->db->query($sql)->bind(':id', $id)->getAll();
        
        return $users;
    }

    public function getAllUsersByGroup($id, $group){
        $sql = "SELECT * FROM korisnik WHERE id != :id AND grupa = :group AND aktivno = 1;";
        $users = $this->db->query($sql)->bind(':id', $id)->bind('group', $group)->getAll();
        
        return $users;
    }

    public function getManagers(){
        $sql = "SELECT * FROM korisnik WHERE grupa = 0 || grupa = 1 AND aktivno = 1;";
        $users = $this->db->query($sql)->getAll();

        return $users;
    }

    public function register(array $userData){
        $sql = 'INSERT INTO korisnik (ime, email, lozinka, grupa) VALUES (:name, :email, :password, :admin)';
        $result = $this->db->query($sql)->bind(':name', $userData['name'])->bind(':email', $userData['email'])->bind(':password', $userData['password'])->bind(':admin', $userData['administrator'])->execute();

        return $result;
    }

    public function login($email, $password){
        $sql = 'SELECT * FROM korisnik WHERE email = :email';
        $row = $this->db->query($sql)->bind(':email', $email)->getRow();

        if($this->db->rowCount() > 0){
            return $row;
        }

        return false;
    }

    public function updateUserData($id, $userData){
        $admin = !empty($userData['administrator']) ? ', grupa = :admin' : '';
        $sql = 'UPDATE korisnik SET ime = :name, email = :email' . $admin . ' WHERE id = :id';
        $result = $this->db->query($sql)->bind(':name', $userData['name'])->bind(':email', $userData['email'])->bind(':id', $id);
        if(!empty($userData['administrator'])){
            $result->bind(':admin', $userData['administrator']);
        }

        $result->execute();

        return $result;
    }

    public function updateUserPassword($id, $userData){
        $sql = 'UPDATE korisnik SET lozinka = :password WHERE id = :id';
        $result = $this->db->query($sql)->bind(':password', $userData['password'])->bind(':id', $id)->execute();

        return $result;
    }

    public function deleteUser($id){
        $sql = 'UPDATE korisnik SET aktivno = 0 WHERE id = :id';
        $result = $this->db->query($sql)->bind(':id', $id)->execute();

        return $result;
    }

    public function setUserData(array $userData){
        foreach($userData as $key => $value){
            if(array_key_exists($key, $this->userData)){
                $this->userData[$key] = $value;
            }
        }
    }

    public function setUserDataError(array $userData){
        foreach($userData as $key => $value){
            if(array_key_exists($key, $this->userDataError)){
                $this->userDataError[$key] = $value;
            }
        }
    }

    public function countUsers(){
        $sql = 'SELECT COUNT(*) AS broj FROM korisnik WHERE aktivno = 1;';
        $result = $this->db->query($sql)->getRow();
        return $result;
    }

    public function getUserData(){
        return $this->userData;
    }

    public function getUserDataError(){
        return $this->userDataError;
    }

    public function getEditUserId(){
        return $this->editUserId;
    }

    public function setEditUserId($id){
        $this->editUserId = $id;
    }

     
}