<?php 


namespace Models;

use Libraries\Core\Database;
use Libraries\Core\FormModel;
use Libraries\Core\Session;

class Product{
    
    private $db;
    private $formModel;
    private $productData;
    private $productDataError;
    private $editProductId;
    private $warehouseId = null;

    const UNITS = [
        'kom',
        'l',
        'dl',
        'ml',
        'kg',
        'g',
        'm',
        'm^2',
    ];

    public function __construct(){
        $this->db = new Database;

        $this->formModel = new FormModel([
            'name' => '',
            'description' => '',
            'quantity' => '',
            'price' => '',
            'category' => '',
        ],[
            'name' => '',
            'quantity' => '',
            'price' => '',
        ]);

        $this->productData = [
            'name' => '',
            'description' => '',
            'quantity' => '',
            'price' => '',
            'category' => '',
            'kalo' => '',
        ];

        $this->productDataError = [
            'name' => '',
            'quantity' => '',
            'price' => '',
            'kalo' => '',
        ];
    }

    public function getById($id, $warehouseId = null){
        $sql = "SELECT proizvod.*,
                       proizvod_kategorija.naziv AS 'kategorija'
                FROM proizvod  
                LEFT JOIN proizvod_kategorija ON proizvod_kategorija.id = proizvod.kategorija_id         
                WHERE proizvod.id = :id AND proizvod.aktivno = 1;";
        $product = $this->db->query($sql)->bind(':id', $id)->getRow();
        
        return $product;
    }

    public function getByName($name){
        $sql = "SELECT * FROM proizvod  WHERE naziv = :name AND aktivno = 1;";
        $product = $this->db->query($sql)->bind(':name', $name)->getRow();
        
        return $product;
    }

    public function getAll(){
        $sql = "SELECT proizvod.*,
                        proizvod_kategorija.naziv AS 'kategorija',
                        sum(zaliha.kolicina) AS kolicina
                FROM proizvod 
                LEFT JOIN zaliha ON zaliha.id_proizvod = proizvod.id AND proizvod.aktivno = 1
                LEFT JOIN skladiste ON skladiste.id = zaliha.id_skladiste and skladiste.aktivno = 1
                JOIN proizvod_kategorija ON proizvod_kategorija.id = proizvod.kategorija_id
                GROUP BY proizvod.id;";
        $product = $this->db->query($sql)->getAll();
        
        return $product;
    }

    public function getAllWarehouseProducts($warehouseId){
        $sql = "SELECT 
                    proizvod.*,
                    zaliha.kolicina,
                    proizvod_kategorija.naziv AS 'kategorija' 
                FROM proizvod
                    LEFT JOIN zaliha ON proizvod.id = zaliha.id_proizvod AND zaliha.id_skladiste = :skladisteId
                    JOIN proizvod_kategorija ON proizvod_kategorija.id = proizvod.kategorija_id
                    JOIN skladiste ON zaliha.id_skladiste = skladiste.id
                WHERE proizvod.aktivno = 1
                    AND skladiste.aktivno = 1;";

        $products = $this->db->query($sql)->bind('skladisteId', $warehouseId)->getAll();
        
        return $products;
    }

    public function getCategoryes(){
        $sql = 'SELECT * FROM proizvod_kategorija WHERE aktivno = 1;';
        $result = $this->db->query($sql)->getAll();

        return $result;
    }

    public function create(array $productData){
        $sql = 'INSERT INTO proizvod (naziv, opis, cijena, kalo, mjerna_jedinica, korisnik_id, kategorija_id) VALUES (:naziv, :opis, :cijena, :kalo, :unit, :korisnik_id, :kategorija_id);';
        $this->db->query($sql)
                            ->bind(':naziv', $productData['name'])
                            ->bind(':opis', $productData['description'])
                            ->bind(':cijena', $productData['price'])
                            ->bind(':kalo', $productData['kalo'])
                            ->bind(':unit', $productData['unit'])
                            ->bind(':korisnik_id', $productData['userId'])
                            ->bind(':kategorija_id', $productData['category'])
                            ->execute();

        $result = $this->db->getLastInsertId();

        return $result;
    }

    public function delete($id, $warehouseId = null){

        if(!empty($warehouseId)){
            $sql = 'DELETE FROM stanje WHERE id_proizvod = :id AND id_skladiste = :warehouseId';
            $result = $this->db->query($sql)->bind(':id', $id)->bind(':warehouseId', $warehouseId)->execute();

            return $result;
        }
        $sql = 'UPDATE proizvod SET aktivno = 0 WHERE id = :id';
        $result = $this->db->query($sql)->bind(':id', $id)->execute();

        return $result;
    }

    public function update($id, $productData){
        $sql = 'UPDATE proizvod SET naziv = :naziv, opis = :opis, cijena = :cijena, kalo = :kalo, mjerna_jedinica = :unit, kategorija_id = :kategorija_id WHERE id = :id';
        $result = $this->db->query($sql)->bind(':naziv', $productData['name'])
                                        ->bind(':opis', $productData['description'])
                                        ->bind(':cijena', $productData['price'])
                                        ->bind(':kalo', $productData['kalo'])
                                        ->bind(':unit', $productData['unit'])
                                        ->bind(':kategorija_id', $productData['category'])
                                        ->bind(':id', $id)
                                        ->execute();

        return $result;
    }

    public function countProducts(){
        $sql = 'SELECT COUNT(*) AS broj FROM proizvod;';
        $result = $this->db->query($sql)->getRow();
        return $result;
    }

    public function getAllWarehouses(){
        $sql = 'SELECT * FROM skladiste WHERE aktivno = 1;';
        $result = $this->db->query($sql)->getAll();
        return $result;
    }

    public function getWarehouseById($warehosueid){
        $sql = 'SELECT * FROM  skladiste WHERE id = :warehouseId AND aktivno = 1';
        $warehouse = $this->db->query($sql)->bind(':warehouseId', $warehosueid)->getRow();

        return $warehouse;
    }

    public function setProductData(array $productData){
        foreach($productData as $key => $value){
            if(array_key_exists($key, $this->productData)){
                $this->productData[$key] = $value;
            }
        }
    }

    public function setProductDataError(array $productData){
        foreach($productData as $key => $value){
            if(array_key_exists($key, $this->productDataError)){
                $this->productDataError[$key] = $value;
            }
        }
    }

    public function getProductData(){
        return $this->productData;
    }

    public function getProductDataError(){
        return $this->productDataError;
    }

    public function getEditProductId(){
        return $this->editProductId;
    }

    public function setEditProductId($id){
        $this->editProductId = $id;
    }

    public function setWarehouseId($id){
        $this->warehouseId = $id;
    }

    public function getWarehouseId(){
        return $this->warehouseId;
    }
     
}