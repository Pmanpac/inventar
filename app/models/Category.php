<?php 


namespace Models;

use Libraries\Core\Database;


class Category{
    
    private $db;
    private $categoryData;
    private $categoryDataError;
    public $editCategoryId;

    public function __construct(){
        $this->db = new Database;

        $this->categoryData = [
            'category_name' => '',
            'category_desc' => '',
        ];

        $this->categoryDataError = [
            'category_name' => '',
        ];
    }

    public function create($naziv, $opis){
        $sql = 'INSERT INTO proizvod_kategorija (naziv, opis) VALUES (:naziv, :opis);';
        $result = $this->db->query($sql)->bind(':naziv', $naziv)->bind(':opis', $opis)->execute();

        return $result;
    }

    public function delete($id){
        $sql = 'UPDATE proizvod_kategorija SET aktivno = 0 WHERE id = :id';
        $result = $this->db->query($sql)->bind(':id', $id)->execute();

        return $result;
    }

    public function update($id, $naziv, $opis){
        $sql = 'UPDATE proizvod_kategorija SET naziv = :naziv, opis = :opis WHERE id = :id';
        $result = $this->db->query($sql)->bind(':id', $id)->bind(':naziv', $naziv)->bind('opis', $opis)->execute();

        return $result;
    }

    public function getByName($name){
        $sql = 'SELECT * FROM proizvod_kategorija WHERE naziv = :name;';
        $result = $this->db->query($sql)->bind(':name', $name)->getRow();

        return $result;
    }

    public function getById($id){
        $sql = 'SELECT * FROM proizvod_kategorija WHERE id = :id;';
        $result = $this->db->query($sql)->bind(':id', $id)->getRow();

        return $result;
    }

    public function readAll(){
        $sql = 'SELECT * FROM proizvod_kategorija WHERE aktivno = 1;';
        $result = $this->db->query($sql)->getAll();

        return $result;
    }

    public function countCategories(){
        $sql = 'SELECT COUNT(*) AS broj FROM proizvod_kategorija;';
        $result = $this->db->query($sql)->getRow();
        return $result;
    }

    public function setCategoryData(array $data){
        foreach($data as $key => $value){
            if(in_array($key, array_keys( $this->categoryData))){
                $this->categoryData[$key] = $value;
            }
        }
    }

    public function setCategoryDataError(array $data){
        foreach($data as $key => $value){
            if(in_array($key, array_keys( $this->categoryDataError))){
                $this->categoryDataError[$key] = $value;
            }
        }
    }

    public function getCategoryData(){
        return $this->categoryData;
    }

    public function getCategoryDataError(){
        return $this->categoryDataError;
    }
}