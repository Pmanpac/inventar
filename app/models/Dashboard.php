<?php 


namespace Models;

use Libraries\Core\Database;


class Dashboard{
    
    
    private $usersModel;
    private $productsModel;
    private $categoryModel;
    private $inventoryModel;

    public function __construct(){
        $this->usersModel = new User();
        $this->productsModel = new Product();
        $this->categoryModel = new Category();
        $this->inventoryModel = new Inventory();
    }

    public function getUsersCount(){
        return $this->usersModel->countUsers();
    }

    public function getProductsCount(){
        return $this->productsModel->countProducts();
    }

    public function getCategoryCount(){
        return $this->categoryModel->countCategories();
    }

    public function getActiveInventoryCount(){
        $inventuraCount = $this->inventoryModel->getActiveInventoryCount();

        return $inventuraCount;
    }

    public function getInventoryReportCount(){
        return $this->inventoryModel->getInventoryReportsCount();
    }

}