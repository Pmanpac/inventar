<?php 

namespace Components\Home;

use Libraries\Core\BaseView;

class View extends BaseView{

    const TEMPLATE = 'template';

    public function renderTemplate(){

        $data = [
            'description' => "Jednostavna web aplikacija za upravljanje vašim skladištem"
        ];
        
        require_once self::TEMPLATE . ".php";
    }

}