<?php

use Libraries\Core\Session;

require_once INCLUDES . "/head.php";
require_once INCLUDES . "/navbar.php"; 

?>

<div class="jumbotron jumbotron-fluid text-center">
        <div class="contrainer">
        <h1 class="display-3">
            Dobrodošli na <?php echo SITE_NAME; ?>
        </h1>
        <p class="lead"><?php echo $data['description'] ?></p>
        </div>
    </div>


<?php require_once INCLUDES . "/footer.php"; ?>