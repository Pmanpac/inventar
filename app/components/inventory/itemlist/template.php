<?php

use Libraries\Core\Session;

require_once INCLUDES . "/head.php";

$session = new Session;
$session->flash('inventory_register_success');
$session->flash('inventory_edit_success');
$session->flash('inventory_delete_success');

require_once INCLUDES . "/sidebar.php";

?>

        <main role="main" class="col-lg-9 ml-sm-auto mt-sm-4 mb-sm-5 col-lg-10 pt-3 px-4">

            <h1> Inventura: <?php echo $inventory->naziv ?> </h1>

			<h3 class="mt-sm-3"> <?php echo $subTitle ?> </h3>

			<div class="btn-float">
				<a target="_blank" href="<?php echo URL_BASE ?>inventory/listpfd/ <?php echo $inventoryListId ?>" class="btn btn-primary mt-2 ">Preuzmi pdf</a>
				<button class="btn btn-primary mt-2">Spremi skladišno stanje</button>
				<div id="advance" class="<?php echo $advance ? 'd-block' : 'd-none';?>">
					<a href="<?php echo URL_BASE . "inventory/summary/" . $inventoryListId ?>" class="btn btn-primary mt-2">Pregled inventurne rezlike</a>
					<a href="<?php echo URL_BASE . 'inventory/finish/' . $inventoryListId ?>" class="btn btn-warning btn-finish mt-2">Zaključi inventuru</a>
				</div>
			</div>
			
			<table id="inventoryList" class="table table-striped table-bordered dt-responsive responsive" style="width:100%">
				<thead>
					<tr>
						<th>ID</th>
						<th>Naziv</th>
						<th>Administrativno stanje</th>
						<th>Skladišno stanje</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($inventoryList as $product): ?>
						<tr>
							<td><?php echo $product->id; ?></td>
							<td><?php echo $product->naziv; ?></td>

							<td><?php echo $product->stanje_sustav; ?></td>
							<td>
								<div class="form-group auto-update">
									<input id="product-<?php echo $product->item_id ?>" type="number" min="0" class="form-control form-control-lg" value="<?php echo $product->stanje_skladiste ?>">
								</div>
								<div class="auto-update-error red-text">

								</div>
							</td>
						</tr>

                    <?php endforeach ?>
				</tbody>
			</table>     
        </main>
    </div>
</div>


<div class="modal fade" id="finish-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Zaključi inventuru</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <strong>Jeste li sigurni da želite Zaključiti inventuru?</strong><br><br>
        Pritiskom na gumb "Potvrdi" inventura će biti zaključena. <br>
        Podatci će biti trajno pohranjeni, te daljne izmjene nisu moguće.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Odustani</button>
        <a id="finish-final-link" href="" type="button" class="btn btn-primary">Potvrdi</a>
      </div>
    </div>
  </div>
</div>




<?php require_once INCLUDES . "/footer.php"; ?>