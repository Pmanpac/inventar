<?php 

namespace Components\Inventory\Itemlist;

use Libraries\Core\BaseView;

class View extends BaseView{

    const TEMPLATE = 'template';

    public function renderTemplate(){

        $inventoryListId = $this->model->getInventoryListId();
        
        $inventory = $this->model->getById($inventoryListId);
        $subTitle = 'Inventuran lista';

        $inventoryList = $this->model->getInventoryList($inventoryListId);

        $warehouse_stock = [];

        foreach($inventoryList as $inventory_item){
            if(empty($inventory_item->stanje_skladiste)){
                $warehouse_stock[] = false;
                continue;
            }

            $warehouse_stock[] = true;
        }

        $advance = false;

        if(!in_array(false, $warehouse_stock)){
            $advance = true;
        }
        
        require_once self::TEMPLATE . ".php";
    }

}