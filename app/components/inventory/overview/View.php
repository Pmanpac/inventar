<?php 

namespace Components\Inventory\Overview;

use Libraries\Core\BaseView;

class View extends BaseView{

    const TEMPLATE = 'template';
    protected $pageTitle = 'Pregled inventure';

    public function renderTemplate(){

        $inventoryData = $this->model->formModel->getFormData();
        $inventoryDataError = $this->model->formModel->getFormDataError();
        $inventories = $this->model->getAllActiveInventory();
        $warehousesTemp = $this->model->getAllWarehouses();

        $warehouses = [];
        foreach($warehousesTemp as $warehouse){
            if ($warehouse->zakljucena || empty($warehouse->vrijeme_pocetka)){
                $warehouses[] = $warehouse;
            }
        }

        //$subTitle =  !$activeInventory ? 'Započnite novu inventuru ' : 'Pregled aktualne inventure';
        
        $data = [
            'inventoryData' => $inventoryData,
            'inventoryDataError' => $inventoryDataError,
        ];
        
        require_once self::TEMPLATE . ".php";
    }

}