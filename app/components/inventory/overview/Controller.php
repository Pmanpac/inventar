<?php

namespace Components\Inventory\Overview;

use Libraries\Core\Session;

class Controller{

    private $model;
    private $session;

    public function __construct($model = null){
        $this->model = $model;

        $this->session = new Session;

        if(!$this->session->exists('userId')){
            header("Location:" . URL_BASE . "user/login");
        }
    }

    public function overview($params){
        if(!empty($this->model) && $_SERVER['REQUEST_METHOD'] == 'POST'){
            $data = $_POST;

            // trim data
            $data = array_map('trim', $data);
            $this->model->formModel->setFromData($data);

            
             // validate data
             if($this->validate($data)){
                 if($this->model->create($data['warehouse'], $data['name'], $data['description'])){
                    $this->session->flash('inventory_register_success', "Uspješno pokrenut postupak inventure", 'alert alert-success');
                    header("Location:" . URL_BASE . 'inventory/overview');
                    die();
                 }else{
                    $this->session->flash('inventory_register_no_warehouse', 'Došlo je do pogreške. Molimo Vas pokušajte kasnije ili kontaktirajte administratora.', 'alert alert-danger');
                 }
                 
            }else{
                $this->session->flash('inventory_register_no_warehouse', 'Molimo Vas pokušajte kasnije ili kontaktirajte administratora.', 'alert alert-danger');
            }
        }
    }


    protected function validate($data){
        $dataValid = true;

        if(empty($data['name'])){
            $this->model->formModel->setFormDataError(['name'=>'Polje Naziv inventure mora biti popunjeno']);
            $dataValid = false;
        }

        if(empty($data['warehouse'])){
            $this->session->flash('inventory_register_no_warehouse', 'Za pokretanje procesa inventure mora biti odabrano skladište', 'alert alert-danger');
            $dataValid = false;
        }

        return $dataValid;
    }

}