<?php

use Libraries\Core\Session;

require_once INCLUDES . "/head.php";

$session = new Session;
$session->flash('inventory_register_success');
$session->flash('inventory_edit_success');
$session->flash('inventory_delete_success');
$session->flash('inventory_correction_success');
$session->flash('inventory_register_no_warehouse');

require_once INCLUDES . "/sidebar.php";

?>

<main role="main" class="col-lg-9 ml-sm-auto mt-sm-4 mb-sm-5 col-lg-10 pt-3 px-4">

  <h1> Inventura </h1>


  <h3>Pokreni novi proces inventure</h3>

  
    <form action="<?php echo URL_BASE; ?>inventory/overview" method="post" class="mt-sm-3">
      <div class="form-group">
        <label for="name">Naziv inventure: <sup>*</sup> </label>
        <input id="name" type="text" name='name' class="form-control form-control-lg  <?php echo !empty($data['inventoryDataError']['name']) ? 'is-invalid' : '' ?> " value="<?php echo $data['inventoryData']['name']; ?>">
        <span class="invalid-feedback"> <?php echo $data['inventoryDataError']['name']; ?> </span>
      </div>

      
      <h3>Skladište</h3>

      <?php if(!empty($warehouses)): ?>
      <select id="warehouseSelectBox" name="warehouse" class="form-select form-select-lg mb-3">
        <option value="0">-</option>
        <?php foreach ($warehouses as $warehosue) : ?>
          <option  value="<?php echo $warehosue->id ?>"><?php echo $warehosue->naziv ?></option>
        <?php endforeach ?>
      </select>
      <?php else: ?>
        <p><b>** Nema dostupnih skladišta. Molimo Vas završite postojeće procese da bi mogli započeti nove.</b></p>
      <?php endif ?>

      <div class="form-group">
        <label for="description"> Opis inventure </label>
        <textarea class="form-control" id="description" name="description" rows="3"><?php echo $data['inventoryData']['description']; ?></textarea>
      </div>

      <div class="row">
        <div class="col">
          <input type="submit" value="Započni novu inventuru" class="btn btn-success ">
        </div>
        <div class="col"></div>
      </div>
    </form>

    <h3 class="mt-4">Aktivni procesi inventure</h3>

    <table id="product_table" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Naziv</th>
                <th>Opis</th>
                <th>Skladište</th>
                <th>Vrijeme početka</th>
                <th>Vrijeme kraja</th>
                <th></th>           
            </tr>
        </thead>
        <tbody>
            <?php foreach ($inventories as $inventory) : ?>
                <tr>
                    <td><?php echo $inventory->id; ?></td>
                    <td><?php echo $inventory->naziv; ?></td>
                    <td><?php echo $inventory->opis; ?></td>
                    <td><?php echo $inventory->skladiste_naziv; ?></td>
                    <td><?php echo $inventory->vrijeme_pocetka; ?></td>
                    <td><?php echo $inventory->vrijeme_kraja ?></td>
                    <td class="table-data-action">            
                        <?php if(!$inventory->zakljucena): ?>
                          <a href="<?php echo URL_BASE . 'inventory/itemlist/' . $inventory->id ?>" class="btn btn-primary">Inventurnu lista</a>
                          <a href="<?php echo URL_BASE . 'inventory/edit/' . $inventory->id ?>" class="btn btn-primary">Izmjeni</a>
                          <a href="<?php echo URL_BASE . 'inventory/delete/' . $inventory->id ?>" class="btn btn-danger btn-delete">Izbriši</a>

                        <?php endif ?>                        
                    </td>
                </tr>

            <?php endforeach ?>
        </tbody>
    </table>

  
      <?php if(false): ?>
    <p>Naziv inventure: <strong><?php echo $activeInventory->naziv ?></strong></p>
    <p>Opis inventure: <strong><?php echo $activeInventory->opis ?></strong></p>
    <p>Početak inventure: <strong><?php $phpdate = strtotime($activeInventory->vrijeme_pocetka);
                                  $formatedTime = date('d. m. Y. H:i', $phpdate);
                                  echo $formatedTime; ?></strong></p>

    <div class="row">

      <a href="<?php echo URL_BASE . 'inventory/itemlist/' . $activeInventory->id ?>" class="btn btn-primary m-2">Generiraj inventurnu listu</a>

      <a href="<?php echo URL_BASE . 'inventory/edit/' . $activeInventory->id ?>" class="btn btn-primary m-2">Izmjeni podatke inventure</a>

      <a href="<?php echo URL_BASE . 'inventory/finish/' . $activeInventory->id ?>" class="btn btn-warning m-2 btn-finish">Zaključi inventuru</a>

      <a href="<?php echo URL_BASE . 'inventory/delete/' . $activeInventory->id ?>" class="btn btn-danger m-2 btn-delete">Izbriši inventuru</a>


    </div>

<?php endif ?>

</main>
</div>
</div>


<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Brisanje Inventure</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Jeste li sigurni da želite izbrisati inventuru?<br>
        Pritiskom na gumb "Potvrdi" inventura će biti tajno izbrisana.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Odustani</button>
        <a id="delete-user-final-link" href="" type="button" class="btn btn-primary">Potvrdi</a>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="finish-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Zaključi inventuru</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <strong>Jeste li sigurni da želite Zaključiti inventuru?</strong><br><br>
        Pritiskom na gumb "Potvrdi" inventura će biti dovršena. <br>
        Podatci će biti trajno pohranjeni, te daljne izmjene nisu moguće.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Odustani</button>
        <a id="finish-final-link" href="" type="button" class="btn btn-primary">Potvrdi</a>
      </div>
    </div>
  </div>
</div>




<?php require_once INCLUDES . "/footer.php"; ?>