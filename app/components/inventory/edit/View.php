<?php 

namespace Components\Inventory\Edit;

use Libraries\Core\BaseView;
use Libraries\Core\Session;

class View extends BaseView{

    const TEMPLATE = 'template';

    public function renderTemplate(){
        
        $inventoryData = $this->model->getById($this->model->getEditInventoryId());
        $inventoryDataError = $this->model->formModel->getFormDataError();
        $editInventoryId = $inventoryData->id;

        $data = [
            'inventoryData' => $inventoryData,
            'inventoryDataError' => $inventoryDataError,
            'editInventoryId' => $editInventoryId,
        ];

        require_once self::TEMPLATE . ".php";
    }

}