<?php

namespace Components\Inventory\Edit;

use Libraries\Core\Session;

class Controller{

    private $model;
    private $session;

    public function __construct($model = null){
        $this->model = $model;

        $this->session = new Session;

        if(!$this->session->exists('userId')){
            header("Location:" . URL_BASE . "user/login");
            die();
        }
    }

    public function edit($prams){

        if(!empty($prams)){
            $id = $prams[0];

            if(!is_int(intval($id))){
                header("Location:" . URL_BASE . "dashboard/welcome");
                die();
            }

            $inventory = $this->model->getById($id);

            if(!$inventory){
                header("Location:" . URL_BASE . "dashboard/welcome");
                die();
            }

            $this->model->setEditInventoryId($id);

            if(!empty($this->model) && $_SERVER['REQUEST_METHOD'] == 'POST'){
                $data = $_POST;
    
                // trim data
                $data = array_map('trim', $data);
                $this->model->formModel->setFromData($data);
    
                
                 // validate data
                 if($this->validate($data)){
                     if($this->model->update($id, $data['name'], $data['description'])){
                        $this->session->flash('inventory_edit_success', "Uspješno ste izmijenili podatke o inventuri", 'alert alert-success');
                        header("Location:" . URL_BASE . 'inventory/overview');
                        die();
                     }
                     
                }
            }

        }else{
            header("Location:" . URL_BASE . "dashboard/welcome");
            die();
        }

    }

    protected function validate($data){
        $dataValid = true;

        if(empty($data['name'])){
            $this->model->formModel->setFormDataError(['name'=>'Polje Naziv inventure mora biti popunjeno']);
            $dataValid = false;
        }

        return $dataValid;
    }
}