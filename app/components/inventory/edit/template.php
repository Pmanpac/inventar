<?php

use Libraries\Core\Session;
$session = new Session;

require_once INCLUDES . "/head.php";
$session->flash("user_data_edit_success"); 
$session->flash("user_data_edit_fail");
$session->flash("user_pass_edit_success"); 
$session->flash("user_pass_edit_fail"); 
require_once INCLUDES . "/sidebar.php";




?>

        <main role="main" class="col-lg-9 ml-sm-auto mt-sm-4 mb-sm-5 col-lg-10 pt-3 px-4">

            <h1> Izmijena inventure </h1>

            <form action="<?php echo URL_BASE; ?>inventory/edit/<?php echo $data['editInventoryId'] ?>" method="post">
                <div class="form-group">
                    <label for="name">Naziv inventure: <sup>*</sup> </label>
                    <input id="name" type="text" name='name' class="form-control form-control-lg  <?php echo !empty($data['inventoryDataError']['name']) ? 'is-invalid' : '' ?> " value="<?php echo $data['inventoryData']->naziv; ?>">
                    <span class="invalid-feedback"> <?php echo $data['inventoryDataError']['name']; ?> </span>
                </div>

                <div class="form-group">
                    <label for="description"> Opis inventure: </label>
                    <textarea class="form-control" id="description" name="description" rows="3"><?php echo $data['inventoryData']->opis; ?></textarea>
                </div>

                <div class="row">
                    <div class="col">
                        <input type="submit" value="Izmijeni podatke Inventure" class="btn btn-success ">
                    </div>
                    <div class="col"></div>
                </div>
            </form>

        </main>
    </div>
</div>




<?php require_once INCLUDES . "/footer.php"; ?>