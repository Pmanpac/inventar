<?php


namespace Components\Inventory\Finish;
use Libraries\Core\Session;

class Controller{

    private $model;
    private $session;

    public function __construct($model = null){
        $this->model = $model;

        $this->session = new Session;

        if(!$this->session->exists('userId')){
            header("Location:" . URL_BASE . "user/login");
        }
    }

    public function finish($params){
        if(!empty($params)){
            $id = $params[0];

            if(!is_int(intval($id))){
                header("Location:" . URL_BASE . "dashboard/welcome");
                die();
            }

            $inventory = $this->model->getById($id);

            if(!$inventory){
                header("Location:" . URL_BASE . "dashboard/welcome");
                die();
            }

            if($this->model->finish($id)){
                $this->session->flash('inventory_finish_success', 'Inventura uspješno zaključena. Izvještaj možete pogledati pritiskom na "Izvještaji" u glavnom izborniku.', "alert alert-success");
                
            }else{
                $this->session->flash('inventory_finish_fail', 'Došlo je do pogreške prilikom zaključivanja inventure. </br> Molimo Vas pokušajte ponovo ili kontaktirajte administratora', "alert alert-danger");
            }


            header("Location:" . URL_BASE . "dashboard/welcome");
            die();

            

        }else{
            header("Location:" . URL_BASE . "dashboard/welcome");
            die();
        }
    }
}