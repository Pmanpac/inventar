<?php


namespace Components\Inventory\Correct;
use Libraries\Core\Session;

class Controller{

    private $model;
    private $session;

    public function __construct($model = null){
        $this->model = $model;

        $this->session = new Session;

        if(!$this->session->exists('userId')){
            header("Location:" . URL_BASE . "user/login");
        }
    }

    public function correct($params){
        if(!empty($params)){
            $id = $params[0];

            if(!is_int(intval($id))){
                header("Location:" . URL_BASE . "dashboard/welcome");
                die();
            }

            $inventory = $this->model->getById($id);

            if(!$inventory){
                header("Location:" . URL_BASE . "dashboard/welcome");
                die();
            }

            if($this->model->correctSystemValues($id)){
                $this->session->flash('inventory_correction_success', 'Uspješno usklađeno stanje u sustavu sa stvarnim stanjem', "alert alert-success");
                
            }else{
                $this->session->flash('inventory_correction_flash', 'Došlo je do pogreške prilikom usklađivanja stanja. </br> Molimo Vas pokušajte ponovo ili kontaktirajte administratora', "alert alert-danger");
            }


            header('Location: ' . URL_BASE . 'inventory/overview');
            die();

            

        }else{
            header("Location:" . URL_BASE . "dashboard/welcome");
            die();
        }
    }
}