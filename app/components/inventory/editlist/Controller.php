<?php 

namespace Components\Inventory\Editlist;

use Libraries\Core\Session;

class Controller{

    private $model;
    private $session;

    public function __construct($model = null){
        $this->model = $model;

        $this->session = new Session;

        if(!$this->session->exists('userId')){
            header("Location:" . URL_BASE . "user/login");
            die();
        }
    }

    public function editlist($prams){

        $data = $_POST;

        if(isset($data['value']) && is_numeric($data['value']) && (int) $data['value'] >= 0 && !empty($data['id']) && is_numeric($data['id'])){
            if($this->model->updateInventoryState($data['id'], $data['value'])){
                echo json_encode(['success' => true]);
                exit();
            }
        }

        echo json_encode(['success' => false]);   
        exit();

    }

}