<?php

namespace Components\Inventory\Delete;

use Libraries\Core\Session;

class Controller{
    private $model;
    private $session;

    public function __construct($model = null)
    {
        $this->model = $model;
        $this->session = new Session;

        if(!$this->session->exists('userId')){
            header('Location: ' . URL_BASE . '/user/login');
            die();
        }
    }


    public function delete($prams){
        if(!empty($prams)){
            $id = $prams[0];

            if(!is_int(intval($id))){
                header("Location:" . URL_BASE . "dashboard/welcome");
                die();
            }

            $inventory = $this->model->getById($id);

            if(!$inventory){
                header("Location:" . URL_BASE . "dashboard/welcome");
                die();
            }

            $deleted = $this->model->delete($id);

            
            $this->session->flash('inventory_delete_success', "Aktivna inventura uspješno izbrisana", "alert alert-success");
            header('Location: ' . URL_BASE . 'inventory/overview');
            die();
            

            

        }
    }
}