<html>

<head>
	<style>
		body {
			font-family: sans-serif;
			font-size: 10pt;
		}

		p {
			margin: 0pt;
		}

		.bottom-spacer-50 {
			margin-bottom: 50px;
		}

		.bottom-spacer-20 {
			margin-bottom: 20px;
		}

		table.items {
			border: 0.1mm solid #000000;
		}

		td {
			vertical-align: top;
		}

		.items td {
			border-left: 0.1mm solid #000000;
			border-right: 0.1mm solid #000000;
		}

		table thead td {
			background-color: #EEEEEE;
			text-align: center;
			border: 0.1mm solid #000000;

		}

		.items td.blanktotal {
			background-color: #EEEEEE;
			border: 0.1mm solid #000000;
			background-color: #FFFFFF;
			border: 0mm none #000000;
			border-top: 0.1mm solid #000000;
			border-right: 0.1mm solid #000000;
		}

		.items td.totals {
			text-align: right;
			border: 0.1mm solid #000000;
		}

		.items td.cost {
			text-align: "."center;
			vertical-align: "bottom";
		}
	</style>
</head>

<body>
	<!--mpdf
<htmlpageheader name="myheader">
<table width="100%"><tr>
<td width="50%" style="color:#0000BB; "><span style="font-weight: bold; font-size: 14pt;"><?php echo $warehouse->naziv ?></span><br /><?php echo $warehouse->adresa ?><br /><?php echo $warehouse->grad ?></td>
</tr></table>
</htmlpageheader>
<htmlpagefooter name="myfooter">
<div style="border-top: 1px solid #000000; font-size: 9pt; text-align: center; padding-top: 3mm; ">
Page {PAGENO} of {nb}
</div>
</htmlpagefooter>
<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
<sethtmlpagefooter name="myfooter" value="on" />
mpdf-->
	<div style="text-align: center;">
		<h2><?php echo  $subTitle ?></h2>
	</div>

	<div class="bottom-spacer-50"></div>
	<div>
		<h3> Inventura: <?php echo $inventory->naziv ?> </h3>
	</div>
	<div>Vrijeme stvaranja dokumenta: <?php echo  $formatedTime ?></div>
	<br />
	<table class="items bottom-spacer-50" width="100%" style="font-size: 8pt; border-collapse: collapse; " cellpadding="9">
		<thead>
			<tr>
				<td width="10%">ID</td>
				<td width="20%">Naziv</td>

				<td width="10%">Sistemsko stanje</td>
				<td width="10%">Skladišno stanje</td>
				<td width="10%">Kalo</td>
				<td width="10%">Razlika stanja</td>
				<td width="10%">Opravdano</td>
				<td width="10%">Cijena</td>
				<td width="10%">Cijena Uk. sistemski</td>
				<td width="10%">Cijena Uk. skladišno</td>
				<td width="10%">Razlika cijene</td>

			</tr>

		</thead>
		<tbody>
			<!-- ITEMS HERE -->

			<?php foreach ($inventoryList as $product) : ?>

				<?php
				$price_total = 	($product->stanje_skladiste * $product->cijena) - ($product->stanje_sustav * $product->cijena);
				$price_total_color = 'style="color: blue;"';
				if($price_total > 0 ){
					$price_total_color = 'style="color: green;"';
				}else if($price_total < 0){
					$price_total_color = 'style="color: red;"';
				}
				
				$total_system = $product->stanje_sustav * $product->cijena;
				$total_warehouse = $product->stanje_skladiste * $product->cijena;

				$stock_total = $product->stanje_skladiste - $product->stanje_sustav;
				$stock_total_color = 'style="color: blue;"';
				if($stock_total > 0 ){
					$stock_total_color = 'style="color: green;"';
				}else if($stock_total < 0){
					$stock_total_color = 'style="color: red;"';
				}

				?>

				<tr>
					<td align="center"><?php echo  $product->id  ?></td>
					<td align="center"><?php echo $product->naziv ?></td>
					<td class="cost"><?php echo $product->stanje_sustav ?></td>
					<td class="cost"><?php echo $product->stanje_skladiste ?></td>
					<td class="cost"><?php echo $product->kalo ?></td>
					<td class="cost"><?php echo $product->difference ?></td>
					<td <?php echo $product->justifyColor; ?> class="cost"><?php echo $product->justify ?></td>
					<td class="cost"><?php echo $product->cijena ?></td>
					<td class="cost"><?php echo $total_system ?></td>
					<td class="cost"><?php echo $total_warehouse ?></td>
					<td <?php echo $price_total_color; ?> class="cost"><?php echo $price_total ?></td>

				</tr>
			<?php endforeach ?>


		</tbody>
	</table>

	<div style="border: 1px solid <?php echo $reportColor ?>; padding:5px;">
		<h3>Izvještaj</h3>
		<p> Suma vrijednost proizvoda po stanju u skladištu: <strong> <?php echo $totalCountedSum ?> </strong></p>
		<p> Suma vrijednost proizvoda po sustavu: <strong> <?php echo $totalSystemSum ?> </strong></p>
		<p> Ukupna razlika suma: <strong style="color: <?php echo $reportColor ?>;"> <?php echo $totalSum ?> </strong></p>
	</div>


</body>

</html>