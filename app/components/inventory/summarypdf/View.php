<?php 

namespace Components\Inventory\Summarypdf;

use Libraries\Core\BaseView;
use Models\Product;

class View extends BaseView{

    const TEMPLATE = 'template';

    public function renderTemplate(){

        $inventoryListId = $this->model->getInventoryListId();
        
        $inventory = $this->model->getById($inventoryListId);
        $subTitle = 'Dokument inventurne razlike';

        $warehouse = $this->model->getWarehouseById($inventoryListId);

        $phpdate = strtotime( $inventory->vrijeme_pocetka ); 
        $formatedTime = date( "d. m. Y. H:i", $phpdate );

        $inventoryList = $this->model->getInventorySummary($inventoryListId);

        $totalSum = 0;
        $totalSystemSum = 0;
        $totalCountedSum = 0;
        $difference = 0;
        $justify = 'DA';
        $justifyColor = 'green';
        foreach ($inventoryList as $product){
            if($product->stanje_sustav == 0){
                $difference = '-';
            }else{
                $difference = ($product->stanje_skladiste / $product->stanje_sustav) * 100;
                $difference = round($difference, 2) . '%';
                if(100 - $product->kalo > $difference){
                    $justify = 'NE';
                    $justifyColor = 'red';
                }
            }
            $product->difference = $difference;
            $product->justify = $justify;
            $product->justifyColor = "style='color: $justifyColor'";
            $totalSystemSum += $product->stanje_sustav * $product->cijena;
            $totalCountedSum += $product->stanje_skladiste * $product->cijena;
        }

        $totalSum = $totalCountedSum - $totalSystemSum;
        $reportColor = 'black';
        $reportColor = $totalSum > 0 ? 'green' : 'red';
 

        $html = '';

        ob_start();
        require_once 'template.php';
        $html .= ob_get_clean();
                
            

        $mpdf = new \Mpdf\Mpdf([
            'mode' => 'UTF-8',
            'margin_left' => 10,
            'margin_right' => 10,
            'margin_top' => 48,
            'margin_bottom' => 25,
            'margin_header' => 10,
            'margin_footer' => 10
        ]);
        
        $mpdf->charset_in = 'utf-8';
        $mpdf->SetTitle($subTitle . ' - ' . $formatedTime);
        $mpdf->SetDisplayMode('fullpage');
        
        $mpdf->WriteHTML($html);
        
        $mpdf->Output();
    }



}