<?php 

namespace Components\Inventory\Listpfd;

use Libraries\Core\BaseView;
use Models\Product;

class View extends BaseView{

    const TEMPLATE = 'template';

    public function renderTemplate(){

        $inventoryListId = $this->model->getInventoryListId();
        
        $inventory = $this->model->getById($inventoryListId);

        $inventoryList = $this->model->getInventoryList($inventoryListId);

        $warehouse = $this->model->getWarehouseById($inventoryListId);

        $phpdate = strtotime( $inventory->vrijeme_pocetka ); 
        $formatedTime = date( "d. m. Y. H:i", $phpdate );

        $lineShort = str_repeat("_", 15);
        $lineLong = str_repeat("_", 25);
        
        $html = '';

        ob_start();
        require_once 'template.php';
        $html .= ob_get_clean();
                
            

        $mpdf = new \Mpdf\Mpdf([
            'mode' => 'UTF-8',
            'margin_left' => 20,
            'margin_right' => 15,
            'margin_top' => 48,
            'margin_bottom' => 25,
            'margin_header' => 10,
            'margin_footer' => 10
        ]);
        
        $mpdf->charset_in='utf-8';
        $mpdf->SetTitle("Inventurna lista - " . $formatedTime);
        $mpdf->SetDisplayMode('fullpage');
        
        $mpdf->WriteHTML($html);
        
        $mpdf->Output();
    }



}