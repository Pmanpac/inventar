<html>

<head>
	<style>
		body {
			font-family: sans-serif;
			font-size: 10pt;
		}

		p {
			margin: 0pt;
		}

		.bottom-spacer-50{
			margin-bottom: 50px;
		}

		.bottom-spacer-20{
			margin-bottom: 20px;
		}

		table.items {
			border: 0.1mm solid #000000;
		}

		td {
			vertical-align: top;
		}

		.items td {
			border-left: 0.1mm solid #000000;
			border-right: 0.1mm solid #000000;
		}

		table thead td {
			background-color: #EEEEEE;
			text-align: center;
			border: 0.1mm solid #000000;

		}

		.items td.blanktotal {
			background-color: #EEEEEE;
			border: 0.1mm solid #000000;
			background-color: #FFFFFF;
			border: 0mm none #000000;
			border-top: 0.1mm solid #000000;
			border-right: 0.1mm solid #000000;
		}

		.items td.totals {
			text-align: right;
			border: 0.1mm solid #000000;
		}

		.items td.cost {
			text-align: "."center;
			vertical-align: "bottom";
		}
	</style>
</head>

<body>
	<!--mpdf
<htmlpageheader name="myheader">
<table width="100%"><tr>
<td width="50%" style="color:#0000BB; "><span style="font-weight: bold; font-size: 14pt;"><?php echo $warehouse->naziv ?></span><br /><?php echo $warehouse->adresa ?><br /><?php echo $warehouse->grad ?></td>
</tr></table>
</htmlpageheader>
<htmlpagefooter name="myfooter">
<div style="border-top: 1px solid #000000; font-size: 9pt; text-align: center; padding-top: 3mm; ">
Page {PAGENO} of {nb}
</div>
</htmlpagefooter>
<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
<sethtmlpagefooter name="myfooter" value="on" />
mpdf-->
	<div>
		<h3> Inventura: <?php echo $inventory->naziv ?> </h3>
	</div>
	<div>Vrijeme stvaranja dokumenta: <?php echo  $formatedTime ?></div>
	<br />
	<table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse; " cellpadding="8">
		<thead>
			<tr>
				<td width="10%">ID</td>
				<td width="15%">Naziv</td>
				<td width="45%">Opis</td>
				<td width="15%">Sistemsko stanje</td>
				<td width="15%">Skladišno stanje</td>

			</tr>
		</thead>
		<tbody>
			<!-- ITEMS HERE -->


			<?php foreach ($inventoryList as $product) : ?>
				<tr>
					<td align="center"><?php echo  $product->id  ?></td>
					<td align="center"><?php echo $product->naziv ?></td>
					<td><?php echo $product->opis ?></td>
					<td class="cost"><?php echo $product->stanje_sustav ?></td>
					<td class="cost"> <?php echo $product->stanje_skladiste ?> </td>
				</tr>
			<?php endforeach ?>

		</tbody>
	</table>
	

</body>

</html>