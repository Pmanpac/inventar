<?php

use Libraries\Core\Session;

require_once INCLUDES . "/head.php";

$session = new Session;
$session->flash('inventory_register_success');
$session->flash('inventory_edit_success');
$session->flash('inventory_delete_success');

require_once INCLUDES . "/sidebar.php";

?>

        <main role="main" class="col-lg-9 ml-sm-auto mt-sm-4 mb-sm-5 col-lg-10 pt-3 px-4">

            <h1> Inventura: <?php echo $inventory->naziv ?> </h1>
			<h4>Skladište: <?php echo $warehouse->naziv ?></h4>
			<h3 class="mt-2 mb-4"> <?php echo $subTitle ?> </h3>

			<a target="_blank" href="<?php echo URL_BASE . "inventory/summarypdf/" . $inventoryListId ?>" class="btn btn-primary mt-2">Generiraj PDF dokument</a>
			
			<table id="inventorySummary" class="table table-striped table-bordered dt-responsive responsive" style="width:100%">
				<thead>
					<tr>
						<th>ID</th>
						<th>Naziv</th>
						<th>Administrativno stanje</th>
						<th>Skladišno stanje</th>
						<th>Kalo</th>
						<th>Razlika</th>
						<th>Opravdano</th>
						<th>Cijena</th>
						<th>Ukupno Administrativno</th>
						<th>Ukupno Skladišno</th>
						<th>Vrijednosna razlika</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($inventoryList as $product): ?>
						<tr>
							<td><?php echo $product->id; ?></td>
							<td><?php echo $product->naziv; ?></td>

							<td><?php echo $product->stanje_sustav; ?></td>
							<td><?php echo $product->stanje_skladiste ?></td>
							<td><?php echo $product->kalo ?>%</td>
							<td><?php echo $product->difference ?></td>
							<td><?php echo $product->justify ?></td>

							<td><?php echo $product->cijena ?></td>

							<td><?php echo ($totalSystem = $product->stanje_sustav * $product->cijena); ?></td>
							<td><?php echo ($totalCounted = $product->stanje_skladiste * $product->cijena) ?></td>

							<td><?php echo ($totalCounted - $totalSystem); ?></td>
							
						</tr>

                    <?php endforeach ?>
				</tbody>
			</table>  
			
			<h2> Izvještaj </h2>
			<div class="col mt-3 border-<?php echo $reportColor ?>">
				<p> Ukupna vrijednost proizvoda po stanju u skladištu: <strong> <?php echo $totalCountedSum ?> </strong></p>
				<p> Ukupna vrijednost proizvoda po sustavu: <strong> <?php echo $totalSystemSum ?> </strong></p>
				<p> Ukupna razlika: <strong style="color: <?php echo $reportColor ?>;"> <?php echo $totalSum ?> </strong></p>
			</div>
        </main>
    </div>
</div>


<script>
	var inventoryDate = "<?php $phpdate = strtotime( $inventory->vrijeme_pocetka ); $formatedTime = date( 'd. m. Y. H:i', $phpdate );  echo $formatedTime; ?>";
	var documentTitle = "<?php echo $inventory->naziv ?>";

	var totalCountedSum = "Ukupna vrijednost proizvoda po stanju u skladištu:  <?php echo $totalCountedSum ?>";
	var totalSystemSum = "Ukupna vrijednost proizvoda po sustavu:  <?php echo $totalSystemSum ?>";
	var totalSum = "Ukupna razlika:  <?php echo $totalSum ?>";
</script>




<?php require_once INCLUDES . "/footer.php"; ?>

<script src="<?php echo URL_BASE?>js/pdf.js"> </script>