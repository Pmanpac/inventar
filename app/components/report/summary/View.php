<?php 

namespace Components\Report\Summary;

use Libraries\Core\BaseView;

class View extends BaseView{

    const TEMPLATE = 'template';

    public function renderTemplate(){

        $inventoryListId = $this->model->inventoryModel->getInventoryListId();
        
        $inventory = $this->model->inventoryModel->getById($inventoryListId);
        $subTitle = 'Dokument inventurne razlike';

        $inventoryList = $this->model->inventoryModel->getInventorySummary($inventoryListId);
        $warehouse = $this->model->inventoryModel->getWarehouseById($inventoryListId);

        $totalSum = 0;
        $totalSystemSum = 0;
        $totalCountedSum = 0;
        $difference = 0;
        $justify = 'DA';
        $justifyColor = 'green';
        foreach ($inventoryList as $product){
            if($product->stanje_sustav == 0){
                $difference = '-';
            }else{
                $difference = ($product->stanje_skladiste / $product->stanje_sustav) * 100;
                $difference = round($difference, 2) . '%';
                if(100 - $product->kalo > $difference){
                    $justify = 'NE';
                    $justifyColor = 'red';
                }
            }
            $product->difference = $difference;
            $product->justify = $justify;
            $product->justifyColor = $justifyColor;

            $totalSystemSum += $product->stanje_sustav * $product->cijena;
            $totalCountedSum += $product->stanje_skladiste * $product->cijena;
        }

        $totalSum = $totalCountedSum - $totalSystemSum;
        $reportColor = 'black';
        $reportColor = $totalSum > 0 ? 'green' : 'red';
        
        require_once self::TEMPLATE . ".php";
    }

}