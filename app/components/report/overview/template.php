<?php

use Libraries\Core\Session;

require_once INCLUDES . "/head.php";
$session = new Session;
$session->flash('product_create_success');
$session->flash('product_delete_success');
$session->flash('category_edit_success');
require_once INCLUDES . "/sidebar.php";


?>

        <main role="main" class="col-lg-9 ml-sm-auto mt-sm-4 mb-sm-5 col-lg-10 pt-3 px-4">

            <h1> Izvještaji </h1>

            <h3 class=" mb-sm-5">Pregled inventura </h3>

            <table id="inventory_report" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Naziv</th>
                        <th>Skladište</th>
                        <th>Opis</th>
                        <th>Datum početka</th>
                        <th>Datum kraja</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($inventories as $inventory): ?>
                            <tr>
                                <td><?php echo $inventory->id; ?></td>
                                <td><?php echo $inventory->naziv; ?></td>
                                <td><?php echo $inventory->skladiste; ?></td>
                                <td><?php echo $inventory->opis; ?></td>
                                <td><?php $phpdate = strtotime( $inventory->vrijeme_pocetka ); $formatedTime = date( 'd. m. Y.', $phpdate );  echo $formatedTime; ?></td>
                                <td><?php $phpdate = strtotime( $inventory->vrijeme_kraja ); $formatedTime = date( 'd. m. Y.', $phpdate );  echo $formatedTime; ?></td>
                                
                                <td>
                                    <a href="<?php echo URL_BASE; ?>report/inventory/<?php echo $inventory->id; ?>" class="btn btn-primary mt-2">Pregled inventure</a>
                                    <a href="<?php echo URL_BASE; ?>report/summary/<?php echo $inventory->id; ?>" class="btn btn-primary mt-2">Pregled inventure razlike</a>
                                </td>
                            </tr>

                    <?php endforeach ?>
                </tbody>
            </table>
        </main>
    </div>
</div>


<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Brisanje Korisnika</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            Jeste li sigurni da želite izbrisati proizvod?<br>
            Pritiskom na gumb "Potvrdi" proizvod će biti trjano izbrisan.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Odustani</button>
        <a id="delete-user-final-link" href="" type="button" class="btn btn-primary">Potvrdi</a>
      </div>
    </div>
  </div>
</div>




<?php require_once INCLUDES . "/footer.php"; ?>