<?php

namespace Components\Report\Overview;

use Libraries\Core\Session;

class Controller{

    private $session;

    public function __construct($model = null){

        $this->session = new Session;

        if(!$this->session->exists('userId')){
            header("Location:" . URL_BASE . "user/login");
        }
    }

}