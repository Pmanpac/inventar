<?php 

namespace Components\Report\Overview;

use Libraries\Core\BaseView;
use Libraries\Core\Session;

class View extends BaseView{

    const TEMPLATE = 'template';

    public function renderTemplate(){

        $inventories = $this->model->inventoryModel->getInventoryReports();
        
        require_once self::TEMPLATE . ".php";
    }

}