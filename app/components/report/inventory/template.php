<?php

use Libraries\Core\Session;

require_once INCLUDES . "/head.php";

$session = new Session;
$session->flash('inventory_register_success');
$session->flash('inventory_edit_success');
$session->flash('inventory_delete_success');

require_once INCLUDES . "/sidebar.php";

?>

        <main role="main" class="col-lg-9 ml-sm-auto mt-sm-4 mb-sm-5 col-lg-10 pt-3 px-4">

            <h1>Inventura: <?php echo $inventory->naziv ?> </h1>
			<h4>Skladište: <?php echo $warehouse->naziv ?></h4>

			<h3 class="mt-sm-3"> <?php echo $subTitle ?> </h3>

			<a target="_blank" href="<?php echo URL_BASE ?>report/inventorypdf/<?php echo $inventoryListId ?>" class="btn btn-primary mt-2 ">Preuzmi pdf</a>
			
			<table id="inventoryReport" class="table table-striped table-bordered dt-responsive responsive" style="width:100%">
				<thead>
					<tr>
						<th>ID</th>
						<th>Naziv</th>
						<th>Administrativno stanje</th>
						<th>Skladišno stanje</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($inventoryList as $product): ?>
						<tr>
							<td><?php echo $product->id; ?></td>
							<td><?php echo $product->naziv; ?></td>

							<td><?php echo $product->stanje_sustav; ?></td>
							<td>
								<div class="form-group auto-update">
									<?php echo $product->stanje_skladiste ?>
								</div>
							</td>
						</tr>

                    <?php endforeach ?>
				</tbody>
			</table>     
        </main>
    </div>
</div>

<script>
	var inventoryDate = "<?php $phpdate = strtotime( $inventory->vrijeme_pocetka ); $formatedTime = date( 'd. m. Y. H:i', $phpdate );  echo $formatedTime; ?>";
	var documentTitle = "<?php echo $inventory->naziv ?>";
</script>


<?php require_once INCLUDES . "/footer.php"; ?>

<script src="<?php echo URL_BASE?>js/pdf.js"> </script>