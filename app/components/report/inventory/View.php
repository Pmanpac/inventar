<?php 

namespace Components\Report\Inventory;

use Libraries\Core\BaseView;

class View extends BaseView{

    const TEMPLATE = 'template';

    public function renderTemplate(){

        $inventoryListId = $this->model->inventoryModel->getInventoryListId();
        
        $inventory = $this->model->inventoryModel->getById($inventoryListId);
        $subTitle = 'Inventuran lista';

        $warehouse = $this->model->inventoryModel->getWarehouseById($inventoryListId);

        $inventoryList = $this->model->inventoryModel->getInventoryList($inventoryListId);
        
        require_once self::TEMPLATE . ".php";
    }

}