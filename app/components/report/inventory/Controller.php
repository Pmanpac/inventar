<?php

namespace Components\Report\Inventory;

use Libraries\Core\Session;

class Controller{

    private $model;
    private $session;

    public function __construct($model = null){
        $this->model = $model;

        $this->session = new Session;

        if(!$this->session->exists('userId')){
            header("Location:" . URL_BASE . "user/login");
        }
    }

    public function inventory($params){
        
        if(!empty($params)){
            $id = $params[0];

            if(!is_int(intval($id))){
                header("Location:" . URL_BASE . "dashboard/welcome");
                die();
            }

            $invetory = $this->model->inventoryModel->getById($id);

            if(!$invetory){
                header("Location:" . URL_BASE . "dashboard/welcome");
                die();
            }

            $this->model->inventoryModel->setInventoryListId($id);

        }
    }

}