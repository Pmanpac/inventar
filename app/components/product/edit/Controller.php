<?php

namespace Components\Product\Edit;

use Libraries\Core\Session;

class Controller{

    private $model;
    private $session;
    private $id;

    public function __construct($model = null){
        $this->model = $model;

        $this->session = new Session;

        if(!$this->session->exists('userId')){
            header("Location:" . URL_BASE . "user/login");
            die();
        }
    }

    public function edit($prams){

        if(!empty($prams)){
            $id = $prams[0];

            if(!is_int(intval($id))){
                header("Location:" . URL_BASE . "dashboard/welcome");
                die();
            }

            $product = $this->model->getById($id);

            if(!$product){
                header("Location:" . URL_BASE . "dashboard/welcome");
                die();
            }

            $this->model->setEditProductId($id);
            $this->id = $id;

            if(!empty($this->model) && $_SERVER['REQUEST_METHOD'] == 'POST'){
                $data = $_POST;
    
                // trim data
                $data = array_map('trim', $data);
    
                
                 // validate data
                 if($this->validateData($data)){
                     if($this->model->update($id, $data)){
                        $this->session->flash('category_edit_success', "Uspješno ste izmijenili proizvod", 'alert alert-success');
                        header("Location:" . URL_BASE . 'product/overview');
                        die();
                     }
                     
                }
            }

        }

    }

    private function validateData($productData){

        $dataValid = true;
        $product = $this->model->getByName($productData['name']);

        if(empty($productData['name'])){
            $this->model->setProductDataError(['name'=>'Polje Naziv Proizvoda mora biti popunjeno']);
            $dataValid = false;
        }else if($product && $product->id != $this->id){
            $this->model->setProductDataError(['name'=>'Istoimeni proizvod već postoji. Naziv proizvoda mora bit jedinstven.']);
            $dataValid = false;
        }
        
        if(empty($productData['price'])){
            $this->model->setProductDataError(['price'=>'Polje cijena mora biti popunjeno']);
            $dataValid = false;
        }else if(floatval($productData['price']) <= 0){
            $this->model->setProductDataError(['price'=>'Polje cijena mora biti pozitivan broj veći od 0']);
            $dataValid = false;
        }

        if($productData['kalo'] == ''){
            $this->model->setProductDataError(['kalo'=>'Polje "Kalo" mora biti popunjeno']);
            $dataValid = false;
        }else if(floatval($productData['kalo']) < 0){
            $this->model->setProductDataError(['kalo'=>'Polje "Kalo" ne može biti manje od 0']);
            $dataValid = false;
        }else if(floatval($productData['kalo']) > 99.99){
            $this->model->setProductDataError(['kalo'=>'Polje "Kalo" mora biti manje od 99.99']);
            $dataValid = false;
        }

        return $dataValid;
    }
}