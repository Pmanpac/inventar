<?php 

namespace Components\Product\Edit;

use Libraries\Core\BaseView;
use Libraries\Core\Session;

class View extends BaseView{

    const TEMPLATE = 'template';

    public function renderTemplate(){
        
        $product = $this->model->getById($this->model->getEditProductId());
        $categories = $this->model->getCategoryes();
        $productData = $this->model->getProductData();
        $productDataError = $this->model->getProductDataError();

        $data = [
            'product' => $product,
            'categories' => $categories,
            'productData' => $productData,
            'productDataError' => $productDataError
        ];

        require_once self::TEMPLATE . ".php";
    }

}