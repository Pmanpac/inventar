<?php

use Libraries\Core\Session;
use Models\Product;

$session = new Session;

require_once INCLUDES . "/head.php";
$session->flash("user_data_edit_success");
$session->flash("user_data_edit_fail");
$session->flash("user_pass_edit_success");
$session->flash("user_pass_edit_fail");
require_once INCLUDES . "/sidebar.php";




?>

<main role="main" class="col-lg-9 ml-sm-auto mt-sm-4 mb-sm-5 col-lg-10 pt-3 px-4">

    <h3 class="mt-sm-5"> Izmijeni proizvod </h3>

    <form action="<?php echo URL_BASE; ?>product/edit/<?php echo $product->id ?>" method="post">
        <div class="form-group">
            <label for="name">Naziv Proizvoda: <sup>*</sup> </label>
            <input type="text" name='name' class="form-control form-control-lg  <?php echo !empty($data['productDataError']['name']) ? 'is-invalid' : '' ?> " value="<?php echo $data['product']->naziv; ?>">
            <span class="invalid-feedback"> <?php echo $data['productDataError']['name']; ?> </span>
        </div>

        <div class="form-group">
            <label for="description"> Opis Proizvoda </label>
            <textarea class="form-control" id="description" name="description" rows="3"><?php echo $data['product']->opis ?></textarea>
        </div>

        <div class="form-group">
            <label for="price">Cijena proizvoda: <sup>*</sup> </label>
            <input type="number" min="0" step="0.01" name='price' class="form-control form-control-lg  <?php echo !empty($data['productDataError']['price']) ? 'is-invalid' : '' ?> " value="<?php echo $data['product']->cijena; ?>">
            <span class="invalid-feedback"> <?php echo $data['productDataError']['price']; ?> </span>
        </div>

        <div class="form-group">
            <label for="price">Kalo: <sup>*</sup> </label>
            <input type="number" min="0" step="0.01" max="99.99" name='kalo' class="form-control form-control-lg  <?php echo !empty($data['productDataError']['kalo']) ? 'is-invalid' : '' ?> " value="<?php echo $data['product']->kalo; ?>">
            <span class="invalid-feedback"> <?php echo $data['productDataError']['kalo']; ?> </span>
        </div>

        <div class="form-group">
            <label for="unit">Mjerna jedinica</label>
            <select id="unit" name="unit" class="form-control">

                <?php foreach (Product::UNITS as $unit) : ?>
                    <option <?php echo ($data['product']->mjerna_jedinica == $unit ? 'selected="selected"' : '') ?> value="<?php echo $unit ?>"><?php echo $unit ?></option>
                <?php endforeach ?>

            </select>
        </div>

        <div class="form-group">
            <label for="category">Kategorija</label>
            <select id="category" name="category" class="form-control">
                <option value="null">-</option>
                <?php if (!empty($data['categories'])) : ?>
                    <?php foreach ($data['categories'] as $category) : ?>
                        <option <?php echo $data['product']->kategorija_id == $category->id ? 'selected' : '' ?> value="<?php echo $category->id ?>"> <?php echo $category->naziv ?> </option>
                    <?php endforeach ?>
                <?php endif ?>
            </select>
        </div>

        <div class="row">
            <div class="col">
                <input type="submit" value="Izmijeni proizvod" class="btn btn-success ">
            </div>
            <div class="col">

            </div>
        </div>
    </form>

</main>
</div>
</div>




<?php require_once INCLUDES . "/footer.php"; ?>