<?php

namespace Components\Product\Overview;

use Libraries\Core\Session;

class Controller{

    private $model;
    private $session;
    private $warehouse = false;

    public function __construct($model = null){
        $this->model = $model;

        $this->session = new Session;

        if(!$this->session->exists('userId')){
            header("Location:" . URL_BASE . "user/login");
        }
    }

    public function overview($params){

        $warehouseId = null;

        if (!empty($params) && is_numeric($params[0]) && !empty($this->model)){
            $warehouseId = $params[0];
            $this->model->setWarehouseId($warehouseId);
            $this->warehouse = true;
        }

        if(!empty($this->model) && $_SERVER['REQUEST_METHOD'] == 'POST'){

            $productData = $_POST;

            // trim data
            $productData = array_map('trim', $productData);
            
            // save data to model
            $this->model->setProductData($productData);


            if($this->validateData($productData)){

                $productData['userId'] = $this->session->get('userId');
                $product = $this->model->create($productData);

                if($product){
                    $this->session->flash('product_create_success', "Uspješno ste dodali novi proizvod " . $productData['name'], 'alert alert-success');
                    header("Location:" . URL_BASE . 'product/overview');
                    die();
                }
            }

        }

        

    }


    private function validateData($productData){

        $dataValid = true;

        if(empty($productData['name'])){
            $this->model->setProductDataError(['name'=>'Polje Naziv Proizvoda mora biti popunjeno']);
            $dataValid = false;
        }else if($this->model->getByName($productData['name'])){
            $this->model->setProductDataError(['name'=>'Istoimeni proizvod već postoji. Naziv proizvoda mora bit jedinstven.']);
            $dataValid = false;
        }
        
        if(empty($productData['price'])){
            $this->model->setProductDataError(['price'=>'Polje cijena mora biti popunjeno']);
            $dataValid = false;
        }else if(floatval($productData['price']) <= 0){
            $this->model->setProductDataError(['price'=>'Polje cijena mora biti pozitivan broj veći od 0']);
            $dataValid = false;
        }

        if($productData['kalo'] == ''){
            $this->model->setProductDataError(['kalo'=>'Polje "Kalo" mora biti popunjeno']);
            $dataValid = false;
        }else if(floatval($productData['kalo']) < 0){
            $this->model->setProductDataError(['kalo'=>'Polje "Kalo" ne može biti manje od 0']);
            $dataValid = false;
        }else if(floatval($productData['kalo']) > 99.99){
            $this->model->setProductDataError(['kalo'=>'Polje "Kalo" mora biti manje od 99.99']);
            $dataValid = false;
        }

        return $dataValid;
    }

}