<?php 

namespace Components\Product\Overview;

use Libraries\Core\BaseView;
use Libraries\Core\Session;

class View extends BaseView{

    const TEMPLATE = 'template';
    protected $pageTitle = 'Pregled proizvoda';

    public function renderTemplate(){

        $products = [];

        $modalText = 'Jeste li sigurni da želite izbrisati proizvod?<br><br>
        Pritiskom na gumb "Potvrdi" proizvod će biti trajano izbrisan za sva skladišta.';

        if($warehouseId = $this->model->getWarehouseId()){
            $products = $this->model->getAllWarehouseProducts($warehouseId);
            $warehouse = $this->model->getWarehouseById($warehouseId);

            if($warehouse->id){
                $modalText = 'Jeste li sigurni da želite izbrisati proizvod iz skladišta "' . $warehouse->naziv .'"?<br><br>
                Pritiskom na gumb "Potvrdi" proizvod će biti trajano izbrisan iz skladišta "' . $warehouse->naziv . '".';
            }

        }else{
            $products = $this->model->getAll();
        }

        
        $warehouses = $this->model->getAllWarehouses();
        $categories = $this->model->getCategoryes();
        $productData = $this->model->getProductData();
        $productDataError = $this->model->getProductDataError();

        

        $data = [
            'categories' => $categories,
            'productData' => $productData,
            'productDataError' => $productDataError
        ];
        
        require_once self::TEMPLATE . ".php";
    }

}