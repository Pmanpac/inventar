<?php

use Libraries\Core\Session;
use Models\Product;

require_once INCLUDES . "/head.php";
$session = new Session;
$session->flash('product_create_success');
$session->flash('product_delete_success');
$session->flash('category_edit_success');
require_once INCLUDES . "/sidebar.php";


?>

<main role="main" class="col-lg-9 ml-sm-auto mt-sm-4 mb-sm-5 col-lg-10 pt-3 px-4">

    <h1> Proizvodi </h1>

    <h4 class=" mb-sm-5">Pregled Proizvoda </h4>

    <form id="selectWarehosue" action="<?php echo URL_BASE; ?>product/overview" method="post">
        <h3>Skladište</h3>
        <select id="warehouseSelectBox" name="warehouse" class="form-select form-select-lg mb-3">
            <option value="">-</option>
            <?php foreach ($warehouses as $warehosue) : ?>
                <option <?php echo $warehosue->id == $warehouseId ?  'selected' : ''; ?> value="<?php echo $warehosue->id ?>"><?php echo $warehosue->naziv ?></option>
            <?php endforeach ?>
        </select>

        <button type="submit" class="btn btn-primary warehouse-submit">Odaberi</button>
    </form>

    <table id="product_table" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Naziv</th>
                <th>Opis</th>
                <th>Kategorija</th>
                <th>Količina</th>
                <th>Mjerna jedinica</th>
                <th>Cijena</th>
                <th>Ukupno</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($products as $product) : ?>
                <tr>
                    <td><?php echo $product->id; ?></td>
                    <td><?php echo $product->naziv; ?></td>
                    <td><?php echo $product->opis; ?></td>
                    <td><?php echo $product->kategorija; ?></td>
                    <td><?php echo (!empty($product->kolicina) ? $product->kolicina : '0'); ?></td>
                    <td><?php echo $product->mjerna_jedinica ?></td>
                    <td><?php echo $product->cijena; ?></td>
                    <td><?php echo $product->cijena * $product->kolicina; ?></td>
                    <td class="table-data-action">                        
                        <a href="<?php echo URL_BASE; ?>product/edit/<?php echo $product->id; ?>" class="btn btn-primary">Izmjeni</a>
                        <a href="<?php echo URL_BASE; ?>product/delete/<?php echo $product->id . '/' . $warehouseId; ?>" class="btn btn-danger btn-delete">Izbriši</a>
                    </td>
                </tr>

            <?php endforeach ?>
        </tbody>
    </table>

    <h4 id="create" class="mt-sm-5"> Dodaj Proizvod </h4>

    <form action="<?php echo URL_BASE; ?>product/overview/<?php echo $warehouseId; ?>" method="post">
        <div class="form-group">
            <label for="name">Naziv Proizvoda: <sup>*</sup> </label>
            <input type="text" name='name' class="form-control form-control-lg  <?php echo !empty($data['productDataError']['name']) ? 'is-invalid' : '' ?> " value="<?php echo $data['productData']['name']; ?>">
            <span class="invalid-feedback"> <?php echo $data['productDataError']['name']; ?> </span>
        </div>

        <div class="form-group">
            <label for="description"> Opis Proizvoda </label>
            <textarea class="form-control" id="description" name="description" rows="3"><?php echo $data['productData']['description'] ?></textarea>
        </div>

        <div class="form-group">
            <label for="price">Cijena proizvoda: <sup>*</sup> </label>
            <input type="number" min="0" step="0.01" name='price' class="form-control form-control-lg  <?php echo !empty($data['productDataError']['price']) ? 'is-invalid' : '' ?> " value="<?php echo $data['productData']['price']; ?>">
            <span class="invalid-feedback"> <?php echo $data['productDataError']['price']; ?> </span>
        </div>

        <div class="form-group">
            <label for="price">Kalo: <sup>*</sup> </label>
            <input type="number" min="0" step="0.01" max="99.99" name='kalo' class="form-control form-control-lg  <?php echo !empty($data['productDataError']['kalo']) ? 'is-invalid' : '' ?> " value="<?php echo $data['productData']['kalo']; ?>">
            <span class="invalid-feedback"> <?php echo $data['productDataError']['kalo']; ?> </span>
        </div>

        <div class="form-group">
            <label for="unit">Mjerna jedinica</label>
            <select id="unit" name="unit" class="form-control">

                <?php foreach (Product::UNITS as $unit) : ?>
                    <option value="<?php echo $unit ?>"><?php echo $unit ?></option>
                <?php endforeach ?>

            </select>
        </div>

        <div class="form-group">
            <label for="category">Kategorija</label>
            <select id="category" name="category" class="form-control">
                <?php if (!empty($data['categories'])) : ?>
                    <?php foreach ($data['categories'] as $category) : ?>
                        <option <?php echo $data['productData']['category'] == $category->id ? 'selected' : '' ?> value="<?php echo $category->id ?>"> <?php echo $category->naziv ?> </option>
                    <?php endforeach ?>
                <?php endif ?>
            </select>
        </div>

        <div class="row">
            <div class="col">
                <input type="submit" value="Dodaj proizvod" class="btn btn-success ">
            </div>
            <div class="col">

            </div>
        </div>
    </form>
</main>
</div>
</div>


<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Brisanje Proizvoda</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php echo $modalText; ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Odustani</button>
                <a id="delete-user-final-link" href="" type="button" class="btn btn-primary">Potvrdi</a>
            </div>
        </div>
    </div>
</div>




<?php require_once INCLUDES . "/footer.php"; ?>