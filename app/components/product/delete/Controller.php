<?php

namespace Components\Product\Delete;

use Libraries\Core\Session;

class Controller{
    private $model;
    private $session;

    public function __construct($model = null)
    {
        $this->model = $model;
        $this->session = new Session;

        if(!$this->session->exists('userId')){
            header('Location: ' . URL_BASE . '/user/login');
            die();
        }
    }


    public function delete($params){
        if(!empty($params)){
            $id = $params[0];

            if(!is_numeric($id)){
                header("Location:" . URL_BASE . "dashboard/welcome");
                die();
            }

            $warehouseId = null;

            if(!empty($params[1])){
                $warehouseId = $params[1];

                if(!is_numeric($warehouseId)){
                    header("Location:" . URL_BASE . "dashboard/welcome");
                    die();
                }

                $warehouse = $this->model->getWarehouseById($warehouseId);

                if(empty($warehouse->id)){
                    header("Location:" . URL_BASE . "dashboard/welcome");
                    die();
                }
            }

            $product = $this->model->getById($id, $warehouseId);

            if(!$product){
                header("Location:" . URL_BASE . "dashboard/welcome");
                die();
            }

            $deleted = $this->model->delete($id, $warehouseId);

            if($deleted){
                $this->session->flash('product_delete_success', "Proizvod uspješno izbrisan", "alert alert-success");
            header('Location: ' . URL_BASE . 'product/overview');
            die();
            }

        }
    }
}