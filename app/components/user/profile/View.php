<?php 

namespace Components\User\Profile;

use Libraries\Core\BaseView;
use Libraries\Core\Session;

class View extends BaseView{

    const TEMPLATE = 'template';

    public function renderTemplate(){

        $session = new Session;
        
        $user = $this->model->getUserById($session->get('userId'));

        $data = [
            'user' => $user,
            'userDataError' => $this->model->getUserDataError()
        ];
        require_once self::TEMPLATE . ".php";
    }

}