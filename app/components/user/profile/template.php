<?php

use Libraries\Core\Session;
$session = new Session;

require_once INCLUDES . "/head.php";
$session->flash("user_data_edit_success"); 
$session->flash("user_data_edit_fail");
$session->flash("user_pass_edit_success"); 
$session->flash("user_pass_edit_fail"); 
require_once INCLUDES . "/sidebar.php";




?>

        <main role="main" class="col-lg-9 ml-sm-auto mt-sm-4 mb-sm-5 col-lg-10 pt-3 px-4">

            <h1 class="mb-sm-4"> Korisnički profil </h1>

            <div class="pl-sm-2">
                <p>Korisničko ime: <strong><?php echo $data['user']->ime ?></strong></p>
                <p>Korisnička e-mail adresa: <strong><?php echo $data['user']->email ?></strong></p>
                <p>Korisnički tip: <strong><?php echo $data['user']->grupa == 2 ? 'Korisnik' : 'Administrator' ?></strong></p>
                <p>Vrijeme registracije: <strong><?php  $phpdate = strtotime( $user->vrijeme_stvaranja ); $formatedTime = date( 'd. m. Y. H:i', $phpdate );  echo $formatedTime; ?></strong></p>

            <a href="<?php echo URL_BASE ?>user/edit/<?php echo $data['user']->id ?>" class="btn btn-primary">Izmjena korisničkih podataka</a>
            </div>
           
            
            
        </main>
    </div>
</div>




<?php require_once INCLUDES . "/footer.php"; ?>