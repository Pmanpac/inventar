<?php

namespace Components\User\Edit;

use Libraries\Core\Session;

class Controller{

    private $model;
    private $session;

    public function __construct($model = null){
        $this->model = $model;

        $this->session = new Session;

        if(!$this->session->exists('userId')){
            header("Location:" . URL_BASE . "user/login");
            die();
        }
    }

    public function edit($params){

        if(!empty($params)){
            // parse params
            $userId = $params[0];
            $editUser = $this->model->getUserById($userId);

            if(!is_int(intval($userId)) || !$editUser){
                header("Location:" . URL_BASE . "dashboard/404");
                die();
            }

            if($this->session->get('userGroup') >= $editUser->grupa && $this->session->get('userId') != $userId){
                $this->session->flash('user_data_edit_no_access', "Niste ovlašteni raditi izmijene", 'alert alert-danger');
                header("Location:" . URL_BASE . "dashboard/welcome");
                die();
            }

            

            if($this->session->get('userGroup') == 1 && $editUser->grupa == 0){
                header("Location:" . URL_BASE . "dashboard/welcome");
                die();
            }

            // if($this->session->get('userGroup') != $editUser->grupa && $this->session->get('userGroup') != 0){
            //     header("Location:" . URL_BASE . "dashboard/welcome");
            //     die();
            // }

            $this->model->setEditUserId($userId);

            // edit user data
            if(!empty($this->model) && $_SERVER['REQUEST_METHOD'] == 'POST'){
                $this->model->setUserData($_POST);
                $userData = $_POST;
        
                // trim data
                $userData = array_map('trim', $userData);

                if(isset($_POST['name'])){

                    if(!empty($userData['editAdmin'])){
                        if(empty($userData['administrator'])){
                            $userData['administrator'] = 2;
                        }else{
                            $userData['administrator'] = 1;
                        }
                        
                    }
        
                    // validate data
                    if($this->validateData($userData)){
        
                        if($this->model->updateUserData($userId, $userData)){
                            if($this->session->get('userId') == $userId){
                                $this->session->set('userEmail', $userData['email']);
                                $this->session->set('userName', $userData['name']);
                            }                            
                            $this->session->flash('user_data_edit_success', "Uspješno ste izmijenili korisničke podatke", 'alert alert-success');
                            header("Location:" . URL_BASE . 'user/edit/' . $userId);
                            die();
                        }else{
                            $this->session->flash('user_data_edit_fail', "Došlo je do greške prilikom izmijene podataka korisnika", 'alert alert-danger');
                            header("Location:" . URL_BASE . 'user/edit/' . $userId);
                            die();
                        }
                    }
                }else{
                    // validate data
                    if($this->validatePassword($userData)){
                        $userData['password'] = password_hash($userData['password'], PASSWORD_DEFAULT);
                        if($this->model->updateUserPassword($userId, $userData)){
                            $this->session->flash('user_pass_edit_success', "Uspješno izmijenjena lozinka", 'alert alert-success');
                            header("Location:" . URL_BASE . 'user/edit/' . $userId);
                            die();
                        }else{
                            $this->session->flash('user_pass_edit_fail', "Došlo je do greške prilikom izmijene lozinke korisnika", 'alert alert-danger');
                            header("Location:" . URL_BASE . 'user/edit/' . $userId);
                            die();
                        }
                    }
                }
            }
        }else{
            header("Location:" . URL_BASE . "dashboard/welcome");
            die();
        }

    }

    private function validateData($userData){

        $dataValid = true;

        if(empty($userData['name'])){
            $this->model->setUserDataError(['name'=>'Polje korisnicko ime mora biti popunjeno']);
            $dataValid = false;
        }

        if(empty($userData['email'])){
            $this->model->setUserDataError(['email'=>'Polje Email mora biti popunjeno']);
            $dataValid = false;
        }else if($this->model->chckEditMailUnique($userData['email'], $this->model->getEditUserId())){
            $this->model->setUserDataError(['email'=>'Email adresa je već zauzeta']);
            $dataValid = false;
        }

        return $dataValid;

    }


    private function validatePassword($userData){

        $dataValid = true;

        if(empty($userData['password'])){
            $this->model->setUserDataError(['password'=>'Polje lozinka mora biti popunjeno']);
            $dataValid = false;
        }else if(strlen($userData['password']) < 6){
            $this->model->setUserDataError(['password'=>'Polje lozinka mora imati više od 6 znakova']);
            $dataValid = false;
        }

        if(empty($userData['confirm_password'])){
            $this->model->setUserDataError(['confirm_password'=>'Polje lozinka mora biti popunjeno']);
            $dataValid = false;
        }else if($userData['confirm_password'] != $userData['password']){
            $this->model->setUserDataError(['confirm_password'=>'Polja lozinka i polje potrvrdi lozinku moraju biti jendaka']);
            $dataValid = false;
        }

        return $dataValid;

    }
}