<?php 

namespace Components\User\Edit;

use Libraries\Core\BaseView;
use Libraries\Core\Session;

class View extends BaseView{

    const TEMPLATE = 'template';

    public function renderTemplate(){
        
        $user = $this->model->getUserById($this->model->getEditUserId());

        $data = [
            'user' => $user,
            'userDataError' => $this->model->getUserDataError()
        ];
        require_once self::TEMPLATE . ".php";
    }

}