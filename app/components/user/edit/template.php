<?php

use Libraries\Core\Session;
$session = new Session;

require_once INCLUDES . "/head.php";
$session->flash("user_data_edit_success"); 
$session->flash("user_data_edit_fail");
$session->flash("user_pass_edit_success"); 
$session->flash("user_pass_edit_fail"); 
require_once INCLUDES . "/sidebar.php";




?>

        <main role="main" class="col-lg-9 ml-sm-auto mt-sm-4 mb-sm-5 col-lg-10 pt-3 px-4">

            <h1> Izmijena korisničkih podataka </h1>

            <form action="<?php echo URL_BASE; ?>user/edit/<?php echo $data['user']->id ?>" method="post" class="mb-sm-4">
                <div class="form-group">
                    <label for="name">Ime: <sup>*</sup> </label>
                    <input type="text" name='name' class="form-control form-control-lg  <?php echo !empty($data['userDataError']['name']) ? 'is-invalid' : '' ?> " value="<?php echo $data['user']->ime; ?>">
                    <span class="invalid-feedback"> <?php echo $data['userDataError']['name']; ?> </span>
                </div>

                <div class="form-group">
                    <label for="email">Email: <sup>*</sup> </label>
                    <input type="email" name='email' class="form-control form-control-lg  <?php echo !empty($data['userDataError']['email']) ? 'is-invalid' : '' ?> " value="<?php echo $data['user']->email; ?>">
                    <span class="invalid-feedback"> <?php echo $data['userDataError']['email']; ?> </span>
                </div>

                <?php if($session->get('userGroup') == 0 && $session->get('userId') != $data['user']->id): ?>
                    <div class="form-group form-check">
                        <input type="checkbox" class="form-check-input" id="administrator" name="administrator" value="off" <?php echo ($data['user']->grupa == 1 ? 'checked' : ''); ?>>
                        <input id='editAdmin' type='hidden' value='on' name='editAdmin'>
                        <label class="form-check-label" for="administrator">Administrator</label>
                    </div>
                <?php endif ?>

                <div class="row">
                    <div class="col">
                        <input type="submit" value="Izmijeni" class="btn btn-success ">
                    </div>
                    <div class="col">
                        <!-- <a href="<?php echo URL_BASE; ?>user/login" class="btn btn-light ">Have an account? Login</a> -->
                    </div>
                </div>
            </form>

            <h3> Izmijena lozinke </h3>

            <form action="<?php echo URL_BASE; ?>user/edit/<?php echo $data['user']->id ?>" method="post">


                <div class="form-group">
                    <label for="password">Lozinka: <sup>*</sup> </label>
                    <input type="password" name='password' class="form-control form-control-lg  <?php echo !empty( $data['userDataError']['password']) ? 'is-invalid' : '' ?> ">
                    <span class="invalid-feedback"> <?php echo $data['userDataError']['password']; ?> </span>
                </div>


                <div class="form-group">
                    <label for="confirm_password"> Potrvrdite lozinku: <sup>*</sup> </label>
                    <input type="password" name='confirm_password' class="form-control form-control-lg  <?php echo !empty($data['userDataError']['confirm_password']) ? 'is-invalid' : '' ?> ">
                    <span class="invalid-feedback"> <?php echo $data['userDataError']['confirm_password']; ?> </span>
                </div>

                <div class="row">
                    <div class="col">
                        <input type="submit" value="Izmijeni" class="btn btn-success ">
                    </div>
                    <div class="col">
                        <!-- <a href="<?php echo URL_BASE; ?>user/login" class="btn btn-light ">Have an account? Login</a> -->
                    </div>
                </div>
            </form>
        </main>
    </div>
</div>




<?php require_once INCLUDES . "/footer.php"; ?>