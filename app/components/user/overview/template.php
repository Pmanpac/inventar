<?php

use Libraries\Core\Session;


require_once INCLUDES . "/head.php";
$session = new Session;
$session->flash('user_register_success');
$session->flash('user_delete_success');
require_once INCLUDES . "/sidebar.php";



?>

        <main role="main" class="col-lg-9 ml-sm-auto mt-sm-4 mb-sm-5 col-lg-10 pt-3 px-4">

            <h1> Korisnici </h1>
            <h3 class=" mb-sm-5">Pregled Korisnika </h3>

            <div class="table-responsive">
                <table id="users_table"  class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>Ime</th>
                            <th>Email</th>
                            <th>Vrijeme Stvaranja</th>
                            <th>Korisnička grupa</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($data['users'] as $user): ?>
                                <tr>
                                    <td><?php echo $user->ime; ?></td>
                                    <td><?php echo $user->email; ?></td>
                                    <td><?php $phpdate = strtotime( $user->vrijeme_stvaranja ); $formatedTime = date( 'd. m. Y. H:i', $phpdate );  echo $formatedTime; ?></td>
                                    <td><?php echo $user->grupa_label ?></td>
                                    <td>
                                        <?php if ($user->editable): ?>

                                        <a href="<?php echo URL_BASE; ?>user/edit/<?php echo $user->id; ?>" class="btn btn-primary">Izmjeni</a>
                                        <a id="delete-<?php echo $user->id; ?>" href="<?php echo URL_BASE; ?>user/delete/<?php echo $user->id; ?>" class="btn btn-danger btn-delete">Izbriši</a>

                                        <?php endif ?>
                                    </td>
                                </tr>

                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>

            <h3> Stvori Korinika </h3>

            <form action="<?php echo URL_BASE; ?>user/overview" method="post">
                <div class="form-group">
                    <label for="name">Ime: <sup>*</sup> </label>
                    <input type="text" name='name' class="form-control form-control-lg  <?php echo !empty($data['userDataError']['name']) ? 'is-invalid' : '' ?> " value="<?php echo $data['userData']['name']; ?>">
                    <span class="invalid-feedback"> <?php echo $data['userDataError']['name']; ?> </span>
                </div>

                <div class="form-group">
                    <label for="email">Email: <sup>*</sup> </label>
                    <input type="email" name='email' class="form-control form-control-lg  <?php echo !empty($data['userDataError']['email']) ? 'is-invalid' : '' ?> " value="<?php echo $data['userData']['email']; ?>">
                    <span class="invalid-feedback"> <?php echo $data['userDataError']['email']; ?> </span>
                </div>


                <div class="form-group">
                    <label for="password">Lozinka: <sup>*</sup> </label>
                    <input type="password" name='password' class="form-control form-control-lg  <?php echo !empty( $data['userDataError']['password']) ? 'is-invalid' : '' ?> " value="<?php echo $data['userData']['password']; ?>">
                    <span class="invalid-feedback"> <?php echo $data['userDataError']['password']; ?> </span>
                </div>


                <div class="form-group">
                    <label for="confirm_password"> Potrvrdite lozinku: <sup>*</sup> </label>
                    <input type="password" name='confirm_password' class="form-control form-control-lg  <?php echo !empty($data['userDataError']['confirm_password']) ? 'is-invalid' : '' ?> " value="<?php echo $data['userData']['confirm_password']; ?>">
                    <span class="invalid-feedback"> <?php echo $data['userDataError']['confirm_password']; ?> </span>
                </div>

                <?php if($session->get('userGroup') == 0): ?>
                    <div class="form-group form-check">
                        <input type="checkbox" class="form-check-input" id="administrator" name="administrator">
                        <label class="form-check-label" for="administrator">Administrator</label>
                    </div>
                <?php endif ?>

                <div class="row">
                    <div class="col">
                        <input type="submit" value="Stvori korisnika" class="btn btn-success ">
                    </div>
                    <div class="col">
                        <!-- <a href="<?php echo URL_BASE; ?>user/login" class="btn btn-light ">Have an account? Login</a> -->
                    </div>
                </div>
            </form>
        </main>
    </div>
</div>


<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Brisanje Korisnika</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            Jeste li sigurni da želite izbrisati korisnika?<br>
            Pritiskom na gumb "Potvrdi" korisnik će biti trjano izbrisan.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Odustani</button>
        <a id="delete-user-final-link" href="<?php echo URL_BASE; ?>user/delete/<?php echo $user->id; ?>" type="button" class="btn btn-primary">Potvrdi</a>
      </div>
    </div>
  </div>
</div>




<?php require_once INCLUDES . "/footer.php"; ?>