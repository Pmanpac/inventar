<?php

namespace Components\User\Overview;

use Libraries\Core\Session;

class Controller{

    private $model;
    private $session;

    public function __construct($model = null){
        $this->model = $model;

        $this->session = new Session;

        if(!$this->session->exists('userId')){
            header("Location:" . URL_BASE . "user/login");
        }
    }

    public function overview($params){

        if(!empty($this->model) && $_SERVER['REQUEST_METHOD'] == 'POST'){
            $this->model->setUserData($_POST);
            $userData = $_POST;

            // trim data
            $userData = array_map('trim', $userData);
            $userData['administrator'] = $userData['administrator'] === "on" ? 1 : 2;

            // validate data
            if($this->validateData($userData)){
                $userData['password'] = password_hash($userData['password'], PASSWORD_DEFAULT);
                if($this->model->register($userData)){
                    $this->session->flash('user_register_success', "Uspješno ste registrirali novog korisnika", 'alert alert-success');
                    header("Location:" . URL_BASE . 'user/overview');
                    die();
                }else{
                    echo "there was some problem registering the user";
                }
            }
        }else if(!empty($params)){
            
        }

    }

    // validacija i provjera ispravnosti unesenih podataka
    private function validateData($userData){

        $dataValid = true;

        if(empty($userData['name'])){
            $this->model->setUserDataError(['name'=>'Polje korisnicko ime mora biti popunjeno']);
            $dataValid = false;
        }

        if(empty($userData['email'])){
            $this->model->setUserDataError(['email'=>'Polje Email mora biti popunjeno']);
            $dataValid = false;
        }else if($this->model->getUserByEmail($userData['email'])){
            $this->model->setUserDataError(['email'=>'Email adresa je već zauzeta']);
            $dataValid = false;
        }
        
        if(empty($userData['password'])){
            $this->model->setUserDataError(['password'=>'Polje lozinka mora biti popunjeno']);
            $dataValid = false;
        }else if(strlen($userData['password']) < 6){
            $this->model->setUserDataError(['password'=>'Polje lozinka mora imati više od 6 znakova']);
            $dataValid = false;
        }

        if(empty($userData['confirm_password'])){
            $this->model->setUserDataError(['confirm_password'=>'Polje lozinka mora biti popunjeno']);
            $dataValid = false;
        }else if($userData['confirm_password'] != $userData['password']){
            $this->model->setUserDataError(['confirm_password'=>'Polja lozinka i polje potrvrdi lozinku moraju biti jendaka']);
            $dataValid = false;
        }

        return $dataValid;

        

    }
}