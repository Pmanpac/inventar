<?php 

namespace Components\User\Overview;

use Libraries\Core\BaseView;
use Libraries\Core\Session;

class View extends BaseView{

    const TEMPLATE = 'template';
    protected $pageTitle = 'Pregled korisnika';

    public function renderTemplate(){
        
        $session = new Session;

        $users =  $this->model->getAllUsers($this->session->get('userId'));
        foreach($users as $user){
            
            // Korisnika po dafaultu ne mozemo mijenjati
            $user->editable = 0;

            // korisnike nize ovlasti mozemo mijenjati
            if($user->grupa > $session->get('userGroup')){
                $user->editable = 1;
            }

            switch($user->grupa){
                case 0:
                    $user->grupa_label = 'Super Administrator';
                    break;
                case 1:
                    $user->grupa_label = 'Administrator';
                    break;
                default:
                    $user->grupa_label = 'Korisnik';
            }

            
        }

        
        $data = [
            'users' => $users,
            'userData' => $this->model->getUserData(),
            'userDataError' => $this->model->getUserDataError()
        ];
        require_once self::TEMPLATE . ".php";
    }

}