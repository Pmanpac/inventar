<?php

namespace Components\User\Logout;

use Libraries\Core\Session;

class Controller{

    private $model;

    public function __construct($model = null){
        $this->model = $model;
    }

    public function logout($params){
        
        $session = new Session;

        $session->remove("userId");
        $session->remove("userEmail");
        $session->remove("userName");
        $session->destroy();

        header("Location:" . URL_BASE . "user/login");

    }
}