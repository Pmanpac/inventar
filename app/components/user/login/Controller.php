<?php

namespace Components\User\Login;

use Libraries\Core\Session;

class Controller{

    private $model;
    private $session;

    public function __construct($model = null){
        $this->model = $model;

        $this->session = new Session;
        if($this->session->exists('userId')){
            header("Location:" . URL_BASE);
        }
    }

    public function login($params){

        if(!empty($this->model) && $_SERVER['REQUEST_METHOD'] == 'POST'){
            $this->model->setUserData($_POST);
            $userData = $_POST;

            // trim data
            $userData = array_map('trim', $userData);

            // login
            $login = $this->model->login($userData["email"], $userData['password']);
            if($login){

                if(password_verify($userData['password'], $login->lozinka)){
                    $this->session->set("userId", $login->id);
                    $this->session->set("userEmail", $login->email);
                    $this->session->set("userName", $login->ime);
                    $this->session->set("userGroup", $login->grupa);
                    $this->session->flash('user_login_success', "Uspješno ste prijavljeni", 'alert alert-success');

                    header("Location:" . URL_BASE . 'dashboard/welcome');
                    die();
                }else{
                    $this->model->setUserDataError(['password'=>'Lozinka nije ispravna']);
                }
                
                
            }else{
                $this->model->setUserDataError(['email'=>'Email adresa nije ispravna']);
            }
        }

    }
}