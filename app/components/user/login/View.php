<?php 

namespace Components\User\Login;

use Libraries\Core\BaseView;

class View extends BaseView{

    const TEMPLATE = 'template';

    public function renderTemplate(){
        $data = [
            'userData' => $this->model->getUserData(),
            'userDataError' => $this->model->getUserDataError()
        ];
        require_once self::TEMPLATE . ".php";
    }

}