<?php

use Libraries\Core\Session;

require_once INCLUDES . "/head.php";
require_once INCLUDES . "/navbar.php";

?>

<div class="row">
    <div class="col-md-6 mx-auto">
        <div class="card card-body bg-light mt-5">
            <h2>Prijava</h2>
            <p>Molimo Vas popunite formu kako bi se prijavili</p>
            <p>Polja označena sa simbolom "*" moraju biti pupunjena</p>
            <form action="<?php echo URL_BASE; ?>user/login" method="post">

                <div class="form-group">
                    <label for="email">Email: <sup>*</sup> </label>
                    <input type="email" name='email' class="form-control form-control-lg  <?php echo !empty($data['userDataError']['email']) ? 'is-invalid' : '' ?> " value="<?php echo $data['userData']['email']; ?>">
                    <span class="invalid-feedback"> <?php echo $data['userDataError']['email']; ?> </span>
                </div>


                <div class="form-group">
                    <label for="password">Lozinka: <sup>*</sup> </label>
                    <input type="password" name='password' class="form-control form-control-lg  <?php echo !empty( $data['userDataError']['password']) ? 'is-invalid' : '' ?> " value="<?php echo $data['userData']['password']; ?>">
                    <span class="invalid-feedback"> <?php echo $data['userDataError']['password']; ?> </span>
                </div>

                <div class="row">
                    <div class="col">
                        <input type="submit" value="Prijava" class="btn btn-success ">
                    </div>
                    <!-- <div class="col">
                        <a href="<?php echo URL_BASE; ?>user/register" class="btn btn-light ">Have an account? Login</a>
                    </div> -->
                </div>
            </form>
        </div>
    </div>
</div>


<?php require_once INCLUDES . "/footer.php"; ?>