<?php

namespace Components\User\Delete;

use Libraries\Core\Session;

class Controller{
    private $model;
    private $session;

    public function __construct($model = null)
    {
        $this->model = $model;
        $this->session = new Session;

        if(!$this->session->exists('userId')){
            header('Location: ' . URL_BASE . '/user/login');
        }
    }


    public function delete($params){
        if(!empty($params)){
            $id = $params[0];

            if(!is_int(intval($id))){
                header("Location:" . URL_BASE . "dashboard/welcome");
                die();
            }

            $editUser = $this->model->getUserById($id);

            if(!$editUser){
                header("Location:" . URL_BASE . "dashboard/welcome");
                die();
            }

            if($this->session->get('userGroup') == 2){
                $this->session->flash('user_delete_fail', "Vaša korisnička grupa nema dozvlu brisanja korisnika", "alert alert-danger");
                header("Location:" . URL_BASE . "dashboard/welcome");
                die();
            }

            
            if($this->session->get('userGroup') == 1 && $editUser->grupa == 0){
                $this->session->flash('user_delete_fail', "Vaša korisnička grupa nema dozvlu brisanja korisnika ", "alert alert-danger");
                header("Location:" . URL_BASE . "dashboard/welcome");
                die();
            }

            if($this->session->get('userGroup') == $editUser->grupa){
                header("Location:" . URL_BASE . "dashboard/welcome");
                die();
            }

            $deleted = $this->model->deleteUser($id);

            
            $this->session->flash('user_delete_success', "Korisnik uspješno izbrisan", "alert alert-success");
            header('Location: ' . URL_BASE . 'user/overview');
            die(); 

        }else{
            header('Location: ' . URL_BASE . 'user/overview');
            die(); 
        }
    }
}