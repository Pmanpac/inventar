<?php

use Libraries\Core\Session;

require_once INCLUDES . "/head.php";
$session = new Session;
$session->flash('category_register_success');
$session->flash('category_delete_success');
require_once INCLUDES . "/sidebar.php";

?>

        <main role="main" class="col-lg-9 ml-sm-auto mt-sm-4 mb-sm-5 col-lg-10 pt-3 px-4">

            <h1> Kategorije proizvoda </h1>

            <h3 class=" mb-sm-5">Pregled Kategorija </h3>

            <table id="product_category_table" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Naziv</th>
                        <th>Opis</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($data['kategorije'] as $kategorija): ?>
                            <tr>
                                <td><?php echo $kategorija->id; ?></td>
                                <td><?php echo $kategorija->naziv; ?></td>
                                <td><?php echo $kategorija->opis; ?></td>
                                <td>
                                    <?php if($kategorija->id != 0): ?>
                                        <a href="<?php echo URL_BASE; ?>category/edit/<?php echo $kategorija->id; ?>" class="btn btn-primary">Izmjeni</a>
                                        <a href="<?php echo URL_BASE; ?>category/delete/<?php echo $kategorija->id; ?>" class="btn btn-danger btn-delete">Izbriši</a>
                                    <?php endif ?>
                                </td>
                            </tr>

                    <?php endforeach ?>
                </tbody>
            </table>

            <h3> Dodaj Kategoriju proizvoda </h3>

            <form action="<?php echo URL_BASE; ?>category/overview" method="post">
                <div class="form-group">
                    <label for="name">Naziv kategorije: <sup>*</sup> </label>
                    <input id="category_name" type="text" name='category_name' class="form-control form-control-lg  <?php echo !empty($data['categoryDataError']['category_name']) ? 'is-invalid' : '' ?> " value="<?php echo $data['categoryData']['category_name']; ?>">
                    <span class="invalid-feedback"> <?php echo $data['categoryDataError']['category_name']; ?> </span>
                </div>

                <div class="form-group">
                    <label for="category_desc"> Opis kategorije </label>
                    <textarea class="form-control" id="category_desc" name="category_desc" rows="3"><?php echo $data['categoryData']['category_desc']; ?></textarea>
                </div>

                <div class="row">
                    <div class="col">
                        <input type="submit" value="Dodaj Kategoriju" class="btn btn-success ">
                    </div>
                    <div class="col"></div>
                </div>
            </form>
        </main>
    </div>
</div>


<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Brisanje Korisnika</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            Jeste li sigurni da želite izbrisati kategoriju?<br>
            Pritiskom na gumb "Potvrdi" kategorija će biti trjano izbrisan.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Odustani</button>
        <a id="delete-user-final-link" href="" type="button" class="btn btn-primary">Potvrdi</a>
      </div>
    </div>
  </div>
</div>




<?php require_once INCLUDES . "/footer.php"; ?>