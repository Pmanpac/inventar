<?php 

namespace Components\Category\Overview;

use Libraries\Core\BaseView;

class View extends BaseView{

    const TEMPLATE = 'template';
    protected $pageTitle = 'Pregled kategorija';

    public function renderTemplate(){

        $kategorije = $this->model->readAll();
        $categoryData = $this->model->getCategoryData();
        $categoryDataError = $this->model->getCategoryDataError();

        $data = [
            'kategorije' => $kategorije,
            'categoryData' => $categoryData,
            'categoryDataError' => $categoryDataError,
        ];
        
        require_once self::TEMPLATE . ".php";
    }

}