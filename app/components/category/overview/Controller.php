<?php

namespace Components\Category\Overview;

use Libraries\Core\Session;

class Controller{

    private $model;
    private $session;

    public function __construct($model = null){
        $this->model = $model;

        $this->session = new Session;

        if(!$this->session->exists('userId')){
            header("Location:" . URL_BASE . "user/login");
        }
    }

    public function overview($params){
        if(!empty($this->model) && $_SERVER['REQUEST_METHOD'] == 'POST'){
            $data = $_POST;

            // trim data
            $data = array_map('trim', $data);
            $this->model->setCategoryData($data);

            
             // validate data
             if($this->validate($data)){
                 if($this->model->create($data['category_name'], $data['category_desc'])){
                    $this->session->flash('category_register_success', "Uspješno ste dodali novu kategoriju", 'alert alert-success');
                    header("Location:" . URL_BASE . 'category/overview');
                    die();
                 }
                 
            }
        }
        
       

    }


    protected function validate($data){
        $dataValid = true;

        if(empty($data['category_name'])){
            $this->model->setCategoryDataError(['category_name'=>'Polje Naziv kategorije mora biti popunjeno']);
            $dataValid = false;
        }else if($this->model->getByName($data['category_name'])){
            $this->model->setCategoryDataError(['category_name'=>'Naziv kategorije već postoji']);
            $dataValid = false;
        }

        return $dataValid;
    }

}