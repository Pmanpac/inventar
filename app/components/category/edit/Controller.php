<?php

namespace Components\Category\Edit;

use Libraries\Core\Session;

class Controller{

    private $model;
    private $session;
    private $category_edit = null;

    public function __construct($model = null){
        $this->model = $model;

        $this->session = new Session;

        if(!$this->session->exists('userId')){
            header("Location:" . URL_BASE . "user/login");
            die();
        }
    }

    public function edit($prams){

        if(!empty($prams)){
            $id = $prams[0];

            if(!is_int(intval($id))){
                header("Location:" . URL_BASE . "dashboard/welcome");
                die();
            }

            $category = $this->model->getById($id);
            $this->category_edit = $category;

            if(!$category){
                header("Location:" . URL_BASE . "dashboard/welcome");
                die();
            }

            if($category->id == 0){
                header("Location:" . URL_BASE . "dashboard/welcome");
                die();
            }

            $this->model->editCategoryId = $id;

            if(!empty($this->model) && $_SERVER['REQUEST_METHOD'] == 'POST'){
                $data = $_POST;
    
                // trim data
                $data = array_map('trim', $data);
                $this->model->setCategoryData($data);
    
                
                 // validate data
                 if($this->validate($data)){
                     if($this->model->update($id, $data['category_name'], $data['category_desc'])){
                        $this->session->flash('category_register_success', "Uspješno ste izmijenili kategoriju", 'alert alert-success');
                        header("Location:" . URL_BASE . 'category/overview');
                        die();
                     }
                     
                }
            }

        }

    }

    protected function validate($data){
        $dataValid = true;

        if(empty($data['category_name'])){
            $this->model->setCategoryDataError(['category_name'=>'Polje Naziv kategorije mora biti popunjeno']);
            $dataValid = false;
        }else if($data['category_name'] != $this->category_edit->naziv && $this->model->getByName($data['category_name'])){
            $this->model->setCategoryDataError(['category_name'=>'Naziv kategorije već postoji']);
            $dataValid = false;
        }

        return $dataValid;
    }
}