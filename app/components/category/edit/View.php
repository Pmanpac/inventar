<?php 

namespace Components\Category\Edit;

use Libraries\Core\BaseView;
use Libraries\Core\Session;

class View extends BaseView{

    const TEMPLATE = 'template';

    public function renderTemplate(){
        
        $categoryData = $this->model->getById($this->model->editCategoryId);
        $categoryDataError = $this->model->getCategoryDataError();
        $editCategoryId = $this->model->editCategoryId;

        $data = [
            'categoryData' => $categoryData,
            'categoryDataError' => $categoryDataError,
            'editCategoryId' => $editCategoryId,
        ];

        require_once self::TEMPLATE . ".php";
    }

}