<?php

use Libraries\Core\Session;
$session = new Session;

require_once INCLUDES . "/head.php";
$session->flash("user_data_edit_success"); 
$session->flash("user_data_edit_fail");
$session->flash("user_pass_edit_success"); 
$session->flash("user_pass_edit_fail"); 
require_once INCLUDES . "/sidebar.php";




?>

        <main role="main" class="col-lg-9 ml-sm-auto mt-sm-4 mb-sm-5 col-lg-10 pt-3 px-4">

            <h1> Izmijena kategorije </h1>

            <form action="<?php echo URL_BASE; ?>category/edit/<?php echo $data['editCategoryId'] ?>" method="post">
                <div class="form-group">
                    <label for="name">Naziv kategorije: <sup>*</sup> </label>
                    <input id="category_name" type="text" name='category_name' class="form-control form-control-lg  <?php echo !empty($data['categoryDataError']['category_name']) ? 'is-invalid' : '' ?> " value="<?php echo $data['categoryData']->naziv; ?>">
                    <span class="invalid-feedback"> <?php echo $data['categoryDataError']['category_name']; ?> </span>
                </div>

                <div class="form-group">
                    <label for="category_desc"> Opis kategorije </label>
                    <textarea class="form-control" id="category_desc" name="category_desc" rows="3"><?php echo $data['categoryData']->opis; ?></textarea>
                </div>

                <div class="row">
                    <div class="col">
                        <input type="submit" value="Izmijeni Kategoriju" class="btn btn-success ">
                    </div>
                    <div class="col"></div>
                </div>
            </form>

        </main>
    </div>
</div>




<?php require_once INCLUDES . "/footer.php"; ?>