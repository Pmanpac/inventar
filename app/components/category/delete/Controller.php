<?php

namespace Components\Category\Delete;

use Libraries\Core\Session;

class Controller{
    private $model;
    private $session;

    public function __construct($model = null)
    {
        $this->model = $model;
        $this->session = new Session;

        if(!$this->session->exists('userId')){
            header('Location: ' . URL_BASE . '/user/login');
            die();
        }
    }


    public function delete($prams){
        if(!empty($prams)){
            $id = $prams[0];

            if(!is_int(intval($id))){
                header("Location:" . URL_BASE . "dashboard/welcome");
                die();
            }

            $category = $this->model->getById($id);

            if(!$category){
                header("Location:" . URL_BASE . "dashboard/welcome");
                die();
            }

            $deleted = $this->model->delete($id);

            
            $this->session->flash('category_delete_success', "Kategorija uspješno izbrisan", "alert alert-success");
            header('Location: ' . URL_BASE . 'category/overview');
            die();
            

            

        }
    }
}