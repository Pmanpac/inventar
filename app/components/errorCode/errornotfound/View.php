<?php

namespace Components\ErrorCode\ErrorNotFound;

use Libraries\Core\BaseView;

class View extends BaseView{

    const TEMPLATE = 'template';
    protected $pageTitle = '404 Page Not Found';

    public function renderTemplate(){
        require_once self::TEMPLATE . ".php";
    }
}