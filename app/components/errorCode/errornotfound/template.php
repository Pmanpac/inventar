<?php

require_once INCLUDES . "/head.php";


?>

<div class="container mt-sm-4">
    <div id="notfound">
        <div class="notfound-404">
            <h1>:(</h1>
        </div>
        <div class="notfound-text">
            <h2>404 - Page not found</h2>
            <p>Stranica koju tražite nije pronađena na ovoj adresi <span class="red-text">"www.<?php echo $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'] ?> "</span>.</p>
            <a href="<?php echo URL_BASE ?>">povratak na naslovnicu</a>
        </div>
    </div>
</div>

<?php require_once INCLUDES . "/footer.php"; ?>