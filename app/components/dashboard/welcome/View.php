<?php 

namespace Components\Dashboard\Welcome;

use Libraries\Core\BaseView;

class View extends BaseView{

    const TEMPLATE = 'template';
    protected $pageTitle = 'Upravljačka Ploča';

    public function renderTemplate(){
        
        $aktivnaInventura = $this->model->getActiveInventoryCount()->broj;

        $data = [
            ['title' => 'Korisnici', 'count' => $this->model->getUsersCount()->broj, 'link_text' => 'Pregled korisnika', 'link' => URL_BASE . 'user/overview'],
            ['title' => 'Aktivna inventura', 'count' => $aktivnaInventura, 'link_text' => $aktivnaInventura == 1 ? 'Pregled inventure' : 'Nova inventura', 'link' => URL_BASE . 'inventory/overview'],
            ['title' => 'Inventurni izvještaji', 'count' => $this->model->getInventoryReportCount()->broj, 'link_text' => 'Pregled inventurnih izvještaja', 'link' => URL_BASE . 'report/overview'],
        ];

        require_once self::TEMPLATE . ".php";
    }

}