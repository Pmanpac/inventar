<?php 

namespace Components\Dashboard\Welcome;

use Libraries\Core\Session;

class Controller{

    private $session;

    public function __construct()
    {
        $this->session = new Session;

        if(!$this->session->exists('userId')){
            header("Location:" . URL_BASE . "user/login");
        }
        
    }

    public function welcome(){}

}