<?php

use Libraries\Core\Session;

require_once INCLUDES . "/head.php";
$session = new Session;
$session->flash('inventory_finish_success');
$session->flash('inventory_finish_fail');
$session->flash('user_data_edit_no_access');
require_once INCLUDES . "/sidebar.php";

?>

        <main role="main" class="col-lg-9 ml-sm-auto col-lg-10 pt-3 px-4">
            <h1>Dobrodošli u sustav za skladište </h1>

            <h3>Pregled informacija </h3>

            <div class="f-box mt-5">

                <?php foreach($data as $card): ?>
                    <div class="card" style="width: 18rem;">
                        <div class="card-body">
                            <h4 class="card-title"><?php echo $card['title'] ?></h4>
                            <p class="card-text"><strong>Broj: <?php echo $card['count'] ?></strong></p>
                            <p></p>
                            <a href="<?php echo $card['link'] ?>" class="btn btn-primary"><?php echo $card['link_text'] ?></a>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>

        </main>
    </div>
</div>




<?php require_once INCLUDES . "/footer.php"; ?>