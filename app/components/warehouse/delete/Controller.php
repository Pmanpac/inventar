<?php

namespace Components\Warehouse\Delete;

use Libraries\Core\Session;

class Controller{

    private $model;
    private $session;
    private $deleteWarehouse = null;

    public function __construct($model = null){
        $this->model = $model;

        $this->session = new Session;

        if(!$this->session->exists('userId')){
            header("Location:" . URL_BASE . "user/login");
        }
    }

    public function delete($params){
        if(!empty($params)){
            $id = $params[0];

            if(!is_int(intval($id))){
                header("Location:" . URL_BASE . "dashboard/welcome");
                die();
            }

            $this->deleteWarehouse = $this->model->getById($id);

            if(!$this->deleteWarehouse){
                header("Location:" . URL_BASE . "dashboard/welcome");
                die();
            }

            if($this->session->get('userGroup') != 0 && $this->deleteWarehouse->upravitelj_id != $this->session->get('userId')){
                header("Location:" . URL_BASE . "dashboard/welcome");
                die();
            }

            $deleted = $this->model->delete($this->deleteWarehouse->id);

            
            $this->session->flash('warehouse_delete_success', "Skladište '" . $this->deleteWarehouse->naziv . "' uspješno  izbrisano", "alert alert-success");
            header('Location: ' . URL_BASE . 'warehouse/overview');
            die(); 

        }else{
            header('Location: ' . URL_BASE . "dashboard/welcome");
            die(); 
        }
    }

}