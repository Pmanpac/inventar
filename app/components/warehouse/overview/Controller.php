<?php

namespace Components\Warehouse\Overview;

use Libraries\Core\Session;

class Controller{

    private $model;
    private $session;

    public function __construct($model = null){
        $this->model = $model;

        $this->session = new Session;

        if(!$this->session->exists('userId')){
            header("Location:" . URL_BASE . "user/login");
        }
    }

    public function overview($params){
        if(!empty($this->model) && $_SERVER['REQUEST_METHOD'] == 'POST'){
            $data = $_POST;

            // trim data
            $data = array_map('trim', $data);
            $this->model->formModel->setFromData($data);

            
             // validate data
             if($this->validate($data)){
                 if($this->model->create($data['name'], $data['city'], $data['pbr'], $data['address'], $data['description'], $data['manager'])){
                    $this->session->flash('warehosue_create_success', "Uspješno ste dodali novo Skladište.", 'alert alert-success');
                    header("Location:" . URL_BASE . 'warehouse/overview');
                    die();
                 }
                 
            }
        }
    }


    protected function validate($data){
        $dataValid = true;

        if(empty($data['name'])){
            $this->model->formModel->setFormDataError(['name'=>'Polje Naziv skladišta mora biti popunjeno']);
            $dataValid = false;
        }else if(count($this->model->getByName($data['name'])) > 0){
            $this->model->formModel->setFormDataError(['name'=>'Polje Naziv skladišta mora biti jedinstveno u sustavu']);
            $dataValid = false;
        }

        if(empty($data['city'])){
            $this->model->formModel->setFormDataError(['city'=>'Polje Grad skladišta mora biti popunjeno']);
            $dataValid = false;
        }

        if(empty($data['address'])){
            $this->model->formModel->setFormDataError(['address'=>'Polje Adresa skladišta mora biti popunjeno']);
            $dataValid = false;
        }

        if(empty($data['pbr'])){
            $this->model->formModel->setFormDataError(['pbr'=>'Polje "Poštanski broj" mora biti popunjeno']);
            $dataValid = false;
        }

        elseif($data['pbr'] >= 100000 || $data['pbr'] < 10000){
            $this->model->formModel->setFormDataError(['pbr'=>'Polje "Poštanski broj" mora imati točno 5 znamenki']);
            $dataValid = false;
        }

        return $dataValid;
    }

}