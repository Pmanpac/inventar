<?php

use Libraries\Core\Session;

require_once INCLUDES . "/head.php";
$session = new Session;
$session->flash('warehosue_create_success');
$session->flash('warehouse_delete_success');
$session->flash('warehouse_edit_success');
require_once INCLUDES . "/sidebar.php";


?>

        <main role="main" class="col-lg-9 ml-sm-auto mt-sm-4 mb-sm-5 col-lg-10 pt-3 px-4">

            <h1> Skladišta </h1>

            <h3 class=" mb-sm-5">Pregled skladišta </h3>

            <table id="inventory_report" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Naziv</th>
                        <th>Grad</th>
                        <th>PBR</th>
                        <th>Adresa</th>
                        <th>Opis</th>
                        <th>Upravitelj</th>
						<th>Email</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($skladista as $skladiste): ?>
                            <tr>
                                <td><?php echo $skladiste->id; ?></td>
                                <td><?php echo $skladiste->naziv; ?></td>
                                <td><?php echo $skladiste->grad; ?></td>
                                <td><?php echo $skladiste->pbr; ?></td>
                                <td><?php echo $skladiste->adresa; ?></td>
                                <td><?php echo $skladiste->opis; ?></td>
								<td><?php echo $skladiste->ime; ?></td>
								<td><?php echo $skladiste->email; ?></td>                                
                                
                                <td>
                                    <?php if ($skladiste->action == true): ?>
										<a href="<?php echo URL_BASE; ?>warehouse/edit/<?php echo $skladiste->id; ?>" class="btn btn-primary">Izmjeni</a>
                                        <a href="<?php echo URL_BASE; ?>warehouse/delete/<?php echo $skladiste->id; ?>" class="btn btn-danger btn-delete">Briši</a>
									<?php endif ?>
                                </td>
                            </tr>

                    <?php endforeach ?>
                </tbody>
            </table>

            <h3> Dodaj novo skaldište </h3>

            <form action="<?php echo URL_BASE; ?>warehouse/overview" method="post">
                <div class="form-group">
                    <label for="name">Naziv skladišta: <sup>*</sup> </label>
                    <input id="name" type="text" name='name' class="form-control form-control-lg  <?php echo !empty($warehouseDataError['name']) ? 'is-invalid' : '' ?> " value="<?php echo $warehouseData['name']; ?>">
                    <span class="invalid-feedback"> <?php echo $warehouseDataError['name']; ?> </span>
                </div>

                <div class="form-group">
                    <label for="manager"> Upravitelj skladišta: </label> <br>
                    <select name="manager" id="manager">
						<?php
						foreach($managers as $id => $manager){
							$selected = $manager->id === $warehouseData['manager'] ? 'selected="selected"' : '';
							echo '<option '. $selected . ' value="' . $manager->id . '">' . $manager->ime . '</option>';
						}
						?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="city">Grad: <sup>*</sup> </label>
                    <input id="city" type="text" name='city' class="form-control form-control-lg  <?php echo !empty($warehouseDataError['city']) ? 'is-invalid' : '' ?> " value="<?php echo $warehouseData['city']; ?>">
                    <span class="invalid-feedback"> <?php echo $warehouseDataError['city']; ?> </span>
                </div>

                <div class="form-group">
                    <label for="pbr">Poštanski broj: <sup>*</sup> </label>
                    <input id="pbr" type="number" name='pbr' class="form-control form-control-lg  <?php echo !empty($warehouseDataError['pbr']) ? 'is-invalid' : '' ?> " value="<?php echo $warehouseData['pbr']; ?>">
                    <span class="invalid-feedback"> <?php echo $warehouseDataError['pbr']; ?> </span>
                </div>

                <div class="form-group">
                    <label for="address">Adresa: <sup>*</sup> </label>
                    <input id="address" type="text" name='address' class="form-control form-control-lg  <?php echo !empty($warehouseDataError['address']) ? 'is-invalid' : '' ?> " value="<?php echo $warehouseData['address']; ?>">
                    <span class="invalid-feedback"> <?php echo $warehouseDataError['address']; ?> </span>
                </div>

                <div class="form-group">
                    <label for="warehouse_desc"> Opis skladišta: </label>
                    <textarea class="form-control" id="description" name="description" rows="3"><?php echo $warehouseData['description']; ?></textarea>
                </div>

                <div class="row">
                    <div class="col">
                        <input type="submit" value="Dodaj Skladište" class="btn btn-success ">
                    </div>
                    <div class="col"></div>
                </div>
            </form>

        </main>
    </div>
</div>


<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Brisanje Skladišta</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            Jeste li sigurni da želite izbrisati skladište?<br>
            Pritiskom na gumb "Potvrdi" skladište će biti trjano izbrisano.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Odustani</button>
        <a id="delete-user-final-link" href="" type="button" class="btn btn-primary">Potvrdi</a>
      </div>
    </div>
  </div>
</div>




<?php require_once INCLUDES . "/footer.php"; ?>