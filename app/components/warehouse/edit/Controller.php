<?php

namespace Components\Warehouse\Edit;

use Libraries\Core\Session;

class Controller{

    private $model;
    private $session;
    private $editWarehouse = null;

    public function __construct($model = null){
        $this->model = $model;

        $this->session = new Session;

        if(!$this->session->exists('userId')){
            header("Location:" . URL_BASE . "user/login");
        }
    }

    public function edit($params){
        if (!empty($params)) {
            $id = $params[0];

            if (!is_int(intval($id))) {
                header("Location:" . URL_BASE . "dashboard/welcome");
                die();
            }

            $this->editWarehouse = $this->model->getById($id);
            $this->model->seteditWarehouseId($this->editWarehouse->id);

            if (!$this->editWarehouse) {
                header("Location:" . URL_BASE . "dashboard/welcome");
                die();
            }

            if ($this->session->get('userGroup') != 0 && $this->editWarehouse->upravitelj_id != $this->session->get('userId')) {
                header("Location:" . URL_BASE . "dashboard/welcome");
                die();
            }


            if (!empty($this->model) && $_SERVER['REQUEST_METHOD'] == 'POST') {
                $this->model->formModel->setFromData($_POST);
                
                $data = $_POST;
        
                // trim data
                $data = array_map('trim', $data);

                if($this->validate($data)){
                    $modified = $this->model->update($this->editWarehouse->id, $data['name'], $data['city'], $data['pbr'], $data['address'], $data['description'], $data['manager']);

            
                    $this->session->flash('warehouse_edit_success', "Skladište uspješno izmijenjeno", "alert alert-success");
                    header('Location: ' . URL_BASE . 'warehouse/overview');
                    die();
                }


                
            }
        }else{
            header('Location: ' . URL_BASE . "dashboard/welcome");
            die();
        }
    }


    protected function validate($data){
        $dataValid = true;

        if(empty($data['name'])){
            $this->model->formModel->setFormDataError(['name'=>'Polje Naziv skladišta mora biti popunjeno']);
            $dataValid = false;
        }else if(!empty($this->model->checkName($this->editWarehouse->id, $data['name']))){
            $this->model->formModel->setFormDataError(['name'=>'Naziv skladišta mora biti jedinstven.']);
            $dataValid = false;
        }

        if(empty($data['city'])){
            $this->model->formModel->setFormDataError(['city'=>'Polje Grad skladišta mora biti popunjeno']);
            $dataValid = false;
        }

        if(empty($data['pbr'])){
            $this->model->formModel->setFormDataError(['pbr'=>'Polje "Poštanski broj" mora biti popunjeno']);
            $dataValid = false;
        }

        elseif($data['pbr'] >= 100000 || $data['pbr'] < 10000){
            $this->model->formModel->setFormDataError(['pbr'=>'Polje "Poštanski broj" mora imati točno 5 znamenki']);
            $dataValid = false;
        }

        if(empty($data['address'])){
            $this->model->formModel->setFormDataError(['address'=>'Polje Adresa skladišta mora biti popunjeno']);
            $dataValid = false;
        }

        return $dataValid;
    }

}