<?php 

namespace Components\Warehouse\Edit;

use Libraries\Core\BaseView;
use Libraries\Core\Session;

class View extends BaseView{
    
    const TEMPLATE = 'template';
    protected $pageTitle = 'Pregled skladišta';

    public function renderTemplate(){

        $warehouseData = (array) $this->model->getById($this->model->getEditWarehouseId());
        $warehouseDataError = $this->model->formModel->getFormDataError();

        $skladista = $this->model->readAll();
        $managers = $this->model->getManagers();

        

        $userId = $this->session->get('userId');
        $user = $this->model->getUserById($userId);

        foreach($skladista as $index => $skladiste){
            if($user->grupa == 0 || $userId == $skladiste->upravitelj_id){
                $skladista[$index] = (object) array_merge((array) $skladiste, ['action' => true]);
                continue;
            }

            $skladista[$index] = (object) array_merge((array) $skladiste, ['action' => false]);
        }
        
        
        require_once self::TEMPLATE . ".php";
    }

}