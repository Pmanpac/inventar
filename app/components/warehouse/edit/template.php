<?php


require_once INCLUDES . "/head.php";

require_once INCLUDES . "/sidebar.php";


?>

        <main role="main" class="col-lg-9 ml-sm-auto mt-sm-4 mb-sm-5 col-lg-10 pt-3 px-4">

            <h1> Skladišta </h1>

            <h3> Izmijeni podatke skladišta </h3>

            <form action="<?php echo URL_BASE; ?>warehouse/edit/<?php echo $warehouseData['id'] ?>" method="post">
                <div class="form-group">
                    <label for="name">Naziv skladišta: <sup>*</sup> </label>
                    <input id="name" type="text" name='name' class="form-control form-control-lg  <?php echo !empty($warehouseDataError['name']) ? 'is-invalid' : '' ?> " value="<?php echo $warehouseData['naziv']; ?>">
                    <span class="invalid-feedback"> <?php echo $warehouseDataError['name']; ?> </span>
                </div>

                <div class="form-group">
                    <label for="manager"> Upravitelj skladišta: </label> <br>
                    <select name="manager" id="manager">
						<?php
						foreach($managers as $id => $manager){
							$selected = $manager->id == $warehouseData['upravitelj_id'] ? 'selected="selected"' : '';
							echo '<option '. $selected . ' value="' . $manager->id . '">' . $manager->ime . '</option>';
						}
						?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="city">Grad: <sup>*</sup> </label>
                    <input id="city" type="text" name='city' class="form-control form-control-lg  <?php echo !empty($warehouseDataError['city']) ? 'is-invalid' : '' ?> " value="<?php echo $warehouseData['grad']; ?>">
                    <span class="invalid-feedback"> <?php echo $warehouseDataError['city']; ?> </span>
                </div>

                <div class="form-group">
                    <label for="pbr">Poštanski broj: <sup>*</sup> </label>
                    <input id="pbr" type="number" name='pbr' class="form-control form-control-lg  <?php echo !empty($warehouseDataError['pbr']) ? 'is-invalid' : '' ?> " value="<?php echo $warehouseData['pbr']; ?>">
                    <span class="invalid-feedback"> <?php echo $warehouseDataError['pbr']; ?> </span>
                </div>

                <div class="form-group">
                    <label for="address">Adresa: <sup>*</sup> </label>
                    <input id="address" type="text" name='address' class="form-control form-control-lg  <?php echo !empty($warehouseDataError['adresa']) ? 'is-invalid' : '' ?> " value="<?php echo $warehouseData['adresa']; ?>">
                    <span class="invalid-feedback"> <?php echo $warehouseDataError['address']; ?> </span>
                </div>

                <div class="form-group">
                    <label for="warehouse_desc"> Opis skladišta: </label>
                    <textarea class="form-control" id="description" name="description" rows="3"><?php echo $warehouseData['opis']; ?></textarea>
                </div>

                <div class="row">
                    <div class="col">
                        <input type="submit" value="Izmijeni podatke Skladišta" class="btn btn-success ">
                    </div>
                    <div class="col"></div>
                </div>
            </form>

        </main>
    </div>
</div>




<?php require_once INCLUDES . "/footer.php"; ?>